<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Contact extends Model
{
    public function FindAll()
    {
        $data = DB::table('tbl_contact')->orderby('ID','ASC')->get();
        return $data;
    }

}
