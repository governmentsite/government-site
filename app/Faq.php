<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Faq extends Model
{
    public function FindAll()
    {
        $data['data'] = DB::table('tbl_faq')->get();
        return $data;
    }

    public function getByID($id)
    {
        $data = DB::table('tbl_faq')->where('ID', $id)->get();
        return $data;



    }

    public function SaveUpdate($data)
    {
            foreach($data as $data_key=>$data_value) {
            $all_faq_data = array(
                'question' => $data_value['question'],
                'answer' => $data_value['answer'],
                'created_date' => getCurrentDate(),

            );

            DB::table('tbl_faq')->insert($all_faq_data);
             }
        }





}
