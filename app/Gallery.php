<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Gallery extends Model
{
    public function FindAll()
    {
        $data['data'] = DB::table('tbl_gallery')->get();
        return $data;
    }

    public function getByID($id)
    {
        $data = DB::table('tbl_gallery')->where('ID', $id)->get();
        return $data;



    }

    public function SaveUpdate($gallery_image,$id = null)
    {
        if($id == "") {

            foreach($gallery_image as $data_key=>$data_value) {
                $all_gallery_images = array(
                    'image' => $data_value['new_name'],
                    'title' => $data_value['title'],
                    'created_date' => getCurrentDate(),

                );
                DB::table('tbl_gallery')->insert($all_gallery_images);
            }
        }
    }




}
