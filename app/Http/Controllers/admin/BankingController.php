<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Banking;
use Illuminate\Support\Facades\DB;

class BankingController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->banking = new Banking();
    }

    public function index()
    {
        return view('admin/dashboard');
    }

    public function ListBankingServices()
    {
        $data = $this->banking->FindAll();
        return view('admin/banking-list',['data'=>$data]);
    }


    public function BankingServiceForm($id=null)
    {
        if($id!==""){
            $data = $this->banking->getByID($id);
            return view('admin/banking-form',['data'=>$data]);
        }else {
            return view('admin/banking-form');
        }
    }

    public function SaveUpdate(Request $request)
    {
        if ($request->file('banking_image') !== null) {
            $file = $request->file('banking_image');
            $destinationPath = public_path() . '/uploads/banking';
            $filename = time() . '_' . $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $fileName = $file->move($destinationPath, $filename);
            $data = array(
                'ID' => $request->input('ID'),
                'banking_title' => $request->input('banking_title'),
                'banking_description' => $request->input('banking_description'),
                'created_date' => getCurrentDate(),
                'banking_image' => $filename
            );

        } else {
            $data = array(
                'ID' => $request->input('ID'),
                'banking_title' => $request->input('banking_title'),
                'banking_description' => $request->input('banking_description'),
                'created_date' => getCurrentDate(),
            );
        }


        if ($data['ID'] != "") {
            $id = $data['ID'];
            $this->banking->SaveUpdate($data, $id);
        } else {
            $this->banking->SaveUpdate($data);
        }

        return redirect('/admin/manage-banking');
    }



    public function UpdateBankingServiceStatus(Request $request)
    {
        $status = $request->input('status');
        $id = $request->input('id');
        if($status!=''){
            DB::table('tbl_banking')->where('ID', $id)->update(['status' => $status]);
        }
    }

    public function DeleteBankingService(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_banking')->where('ID', $id)->get();
        unlink('uploads/banking/'.$result[0]->banking_image);
        $result = DB::table('tbl_banking')->where('ID', $id)->delete();
        if ($result){
            return  redirect('admin/manage-banking');
        }
    }

}
