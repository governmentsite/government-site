<?php

namespace App\Http\Controllers\admin;

use App\Contact;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->contact = new Contact();
    }

    public function index()
    {
        $data = $this->contact->FindAll();
        return view('admin/contact-list',['data'=>$data]);
    }

    public function DeleteContact(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_contact')->where('ID', $id)->delete();
        if ($result){
            return  redirect('admin/contact-list');
        }
    }

}
