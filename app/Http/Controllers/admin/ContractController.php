<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contract;
use Illuminate\Support\Facades\DB;

class ContractController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->contract = new Contract();
    }

    public function index()
    {
        return view('admin/dashboard');
    }

    public function ListContractServices()
    {
        $data = $this->contract->FindAll();
        return view('admin/contract-list',['data'=>$data]);
    }


    public function ContractServiceForm($id=null)
    {
        if($id!==""){
            $data = $this->contract->getByID($id);
            return view('admin/contract-form',['data'=>$data]);
        }else {
            return view('admin/contract-form');
        }
    }

    public function SaveUpdate(Request $request)
    {
        if ($request->file('contract_image') !== null) {
            $file = $request->file('contract_image');
            $destinationPath = public_path() . '/uploads/contract';
            $filename = time() . '_' . $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $fileName = $file->move($destinationPath, $filename);
            $data = array(
                'ID' => $request->input('ID'),
                'contract_title' => $request->input('contract_title'),
                'contract_description' => $request->input('contract_description'),
                'created_date' => getCurrentDate(),
                'contract_image' => $filename
            );

        } else {
            $data = array(
                'ID' => $request->input('ID'),
                'contract_title' => $request->input('contract_title'),
                'contract_description' => $request->input('contract_description'),
                'created_date' => getCurrentDate(),
            );
        }


        if ($data['ID'] != "") {
            $id = $data['ID'];
            $this->contract->SaveUpdate($data, $id);
        } else {
            $this->contract->SaveUpdate($data);
        }

        return redirect('/admin/manage-contract');
    }



    public function UpdateContractServiceStatus(Request $request)
    {
        $status = $request->input('status');
        $id = $request->input('id');
        if($status!=''){
            DB::table('tbl_contract')->where('ID', $id)->update(['status' => $status]);
        }
    }

    public function DeleteContractService(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_contract')->where('ID', $id)->get();
        unlink('uploads/contract/'.$result[0]->contract_image);
        $result = DB::table('tbl_contract')->where('ID', $id)->delete();
        if ($result){
            return  redirect('admin/manage-contract');
        }
    }

}
