<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Faq;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Foundation\Http\FormRequest;

class FaqController extends Controller
{
    private $faq;
    public function __construct()
    {
        $this->faq = new Faq();
    }



    public function ListTeam()
    {
        $data = $this->faq->FindAll();
        return view('admin/faq-list',$data);
    }

    public function FaqForm($id=null)
    {
        if($id!==""){
            $data = $this->faq->FindAll();
            return view('admin/faq',['data'=>$data]);
        }else {
            return view('admin/faq');
        }
    }


    public function SaveUpdate(Request $request)
    {
        $data=array();
        foreach ($request->input('question') as $data_key => $data_value) {
            $data[] = array(
                'ID' => $request->input('ID')[$data_key],
                'question' => $request->input('question')[$data_key],
                'answer' => $request->input('answer')[$data_key],
                'created_date' => getCurrentDate()[$data_key],
            );
        }

        $this->faq->SaveUpdate($data);


        return redirect('/admin/add-faq');

    }

    public function UpdateTeam($id)
    {
        $data = $this->faq->getByID($id);
        return view('admin/faq',['data'=>$data]);

    }

    public function DeletePracticeArea(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_practice_area')->where('ID', $id)->delete();
        if ($result){
            return redirect()->back()->with('message', 'IT WORKS!');
        }

    }

    public function DeleteFaq(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_faq')->where('ID', $id)->delete();
        if ($result){
            return  redirect('admin/add-faq');
        }
    }



}
