<?php

namespace App\Http\Controllers\admin;

use App\Listing;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ListingController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->listing = new Listing();
    }

    public function index()
    {
        return view('admin/manage-listing');
    }

    public function ListAll()
    {
        $data = $this->listing->FindAll();
        return view('admin/listing',['data'=>$data]);
    }


    public function ListingForm($id=null)
    {
        $data['category'] = $this->listing->FindCategory();
        if($id !==null){
            $data['data'] = $this->listing->getByID($id);

            return view('admin/listing-form',$data);
        }else {
            return view('admin/listing-form',$data);
        }
    }

    public function SaveUpdate(Request $request)
    {
        $data = array(
            'ID' => $request->input('ID'),
            'title' => $request->input('title'),
            'country' => $request->input('country'),
            'region' => $request->input('region'),
            'category' => $request->input('category'),
            'created_date' => getCurrentDate(),
        );


        if ($data['ID'] != "") {
            $id = $data['ID'];
            $this->listing->SaveUpdate($data, $id);
        } else {
            $this->listing->SaveUpdate($data);
        }

        return redirect('/admin/manage-listing');

    }

    public function UpdateListingStatus(Request $request)
    {
        $status = $request->input('status');
        $id = $request->input('id');
        if($status!=''){
            DB::table('tbl_listing')->where('ID', $id)->update(['status' => $status]);
        }
    }

    public function UpdateListingFeatureStatus(Request $request)
    {
        $status = $request->input('status');
        $id = $request->input('id');
        if($status!=''){
            DB::table('tbl_listing')->where('ID', $id)->update(['is_featured' => $status]);
        }
    }

    public function DeleteListing(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_listing')->where('ID', $id)->delete();
        if ($result){
            return  redirect('admin/manage-listing');
        }
    }

}
