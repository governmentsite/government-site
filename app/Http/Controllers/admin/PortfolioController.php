<?php

namespace App\Http\Controllers\admin;

use App\Listing;
use App\Portfolio;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PortfolioController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->portfolio = new Portfolio();
        $this->listing = new Listing();
    }

    public function index()
    {
        return view('admin/manage-portfolio');
    }

    public function ListPortfolio()
    {
        $data = $this->portfolio->FindAll();
        return view('admin/portfolio',['data'=>$data]);
    }


    public function PortfolioForm($id=null)
    {
        $data['listing'] = $this->portfolio->getCategoryAndService();
        if($id !==null){
            $data['data'] = $this->portfolio->getByID($id);
            return view('admin/portfolio-form',$data);
        }else {
            return view('admin/portfolio-form',$data);
        }
    }

    public function SaveUpdate(Request $request)
    {

        if ($request->file('portfolio_image')) {
            $file = $request->file('portfolio_image');
            $destinationPath = public_path() . '/uploads/portfolios';
            $filename = time() . '_' . $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $fileName = $file->move($destinationPath, $filename);
            $data = array(
                'ID' => $request->input('ID'),
                'category' => $request->input('category'),
                'portfolio_title' => $request->input('portfolio_title'),
                'portfolio_description' => $request->input('portfolio_description'),
                'created_date' => getCurrentDate(),
                'portfolio_image'=>$filename
            );
        }
        else{
            $data = array(
                'ID' => $request->input('ID'),
                'category' => $request->input('category'),
                'portfolio_title' => $request->input('portfolio_title'),
                'portfolio_description' => $request->input('portfolio_description'),
                'created_date' => getCurrentDate(),
            );

        }

        if ($data['ID'] != "") {
            $id = $data['ID'];
            $this->portfolio->SaveUpdate($data, $id);
        } else {
            $this->portfolio->SaveUpdate($data);
        }

        return redirect('/admin/manage-portfolio');

    }

    public function UpdatePortfolioStatus(Request $request)
    {
        $status = $request->input('status');
        $id = $request->input('id');
        if($status!=''){
            DB::table('tbl_portfolio')->where('ID', $id)->update(['status' => $status]);
        }
    }

    public function DeletePortfolio(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_portfolio')->where('ID', $id)->delete();
        if ($result){
            return  redirect('admin/manage-portfolio');
        }
    }


}
