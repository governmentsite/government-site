<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Team;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Foundation\Http\FormRequest;

class TeamController extends Controller
{
    private $team;
    public function __construct()
    {
        $this->team = new Team();
    }



    public function ListTeam()
    {
        $data = $this->team->FindAll();
        return view('admin/team-list',$data);
    }

    public function TeamForm($id=null)
    {
        if($id!==""){
            $data['data'] = $this->team->getByID($id);
            $data['practiceAreas'] = $this->team->getPracticeAreas($id);
            return view('admin/team-form',$data);
        }else {
            $data['data']= null;
            return view('admin/team-form',$data);
        }
    }


    public function SaveUpdate(Request $request)
    {


        if ($request->file('member_image')!==null) {
            $file = $request->file('member_image');
            $destinationPath = public_path() . '/uploads/team';
            $filename = time() . '_' . $file->getClientOriginalName();
            $filename = str_replace(' ', '_', $filename);
            $fileName = $file->move($destinationPath, $filename);
            $data = array(
                'ID' => $request->input('ID'),
                'member_name' => $request->input('member_name'),
                'member_category' => $request->input('member_category'),
                'member_description' => $request->input('member_description'),
                'member_designation' => $request->input('member_designation'),
                'member_location' => $request->input('member_location'),
                'member_email' => $request->input('member_email'),
                'member_phone' => $request->input('member_phone'),
                'member_biography' => $request->input('member_biography'),
                'created_date' => getCurrentDate(),
                'member_image'=>$filename
            );

        }else {
            $data = array(
                'ID' => $request->input('ID'),
                'member_name' => $request->input('member_name'),
                'member_category' => $request->input('member_category'),
                'member_description' => $request->input('member_description'),
                'member_designation' => $request->input('member_designation'),
                'member_location' => $request->input('member_location'),
                'member_email' => $request->input('member_email'),
                'member_phone' => $request->input('member_phone'),
                'member_biography' => $request->input('member_biography'),
                'created_date' => getCurrentDate(),
            );
        }
        $practiceArea['practice_area_title'] = array();
        $practiceAreaData = array(
            'practice_area_title' => $request->input('practice_area_title'),
            'practice_area_description' => $request->input('practice_area_description'),
        );


        if ($data['ID'] != "") {
            $id = $data['ID'];
            $this->team->SaveUpdate($data,$practiceAreaData, $id);
        } else {
            $this->team->SaveUpdate($data,$practiceAreaData);
        }


        return redirect('/admin/team-list');

    }

    public function UpdateTeam($id)
    {
        $data = $this->team->getByID($id);
        return view('admin/team',['data'=>$data]);

    }

    public function DeletePracticeArea(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_practice_area')->where('ID', $id)->delete();
        if ($result){
            return redirect()->back()->with('message', 'IT WORKS!');
        }

    }

    public function DeleteTeam(Request $request)
    {
        $id = $request->input('id');
        $result = DB::table('tbl_team')->where('ID', $id)->delete();
        if ($result){
            return  redirect('admin/team-list');
        }
    }

    public static function StaticTeam()
    {
        $data = DB::table('tbl_team')->orderby('ID','DESC')->get();
        return $data;
    }



}
