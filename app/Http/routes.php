<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/admin', 'admin\LoginController@index');
Route::post('/admin/authenticate', 'admin\LoginController@Authenticate');
Route::get('/admin/logout', 'admin\LoginController@logout');


/*PageController*/
Route::get('/admin/dashboard', 'admin\PageController@index');
Route::get('/admin/manage-pages', 'admin\PageController@ListPages');
Route::get('/admin/add-page', 'admin\PageController@PageForm');
Route::get('/admin/add-page/{id}', 'admin\PageController@PageForm');
Route::post('/admin/page-save-update', 'admin\PageController@SaveUpdate');
Route::post('/admin/update-publish-status-page', 'admin\PageController@UpdatePageStatus');
Route::post('/admin/delete-page', 'admin\PageController@DeletePage');


/*DomesticServiceController*/
Route::get('/admin/manage-domestic', 'admin\DomesticServiceController@ListDomesticServices');
Route::get('/admin/add-domestic', 'admin\DomesticServiceController@DomesticServiceForm');
Route::get('/admin/add-domestic/{id}', 'admin\DomesticServiceController@DomesticServiceForm');
Route::post('/admin/domestic-save-update', 'admin\DomesticServiceController@SaveUpdate');
Route::post('/admin/update-publish-status-domestic-service', 'admin\DomesticServiceController@UpdateDomesticServiceStatus');
Route::post('/admin/delete-domestic-service', 'admin\DomesticServiceController@DeleteDomesticService');

/*InternationalServiceController*/
Route::get('/admin/manage-international', 'admin\InternationalServiceController@ListInternationalServices');
Route::get('/admin/add-international', 'admin\InternationalServiceController@InternationalServiceForm');
Route::get('/admin/add-international/{id}', 'admin\InternationalServiceController@InternationalServiceForm');
Route::post('/admin/international-save-update', 'admin\InternationalServiceController@SaveUpdate');
Route::post('/admin/update-publish-status-international-service', 'admin\InternationalServiceController@UpdateInternationalServiceStatus');
Route::post('/admin/delete-international-service', 'admin\InternationalServiceController@DeleteInternationalService');


/*ContractServiceController*/
Route::get('/admin/manage-contract', 'admin\ContractController@ListContractServices');
Route::get('/admin/add-contract', 'admin\ContractController@ContractServiceForm');
Route::get('/admin/add-contract/{id}', 'admin\ContractController@ContractServiceForm');
Route::post('/admin/contract-save-update', 'admin\ContractController@SaveUpdate');
Route::post('/admin/update-publish-status-contract-service', 'admin\ContractController@UpdateContractServiceStatus');
Route::post('/admin/delete-contract-service', 'admin\ContractController@DeleteContractService');


/*BankingController*/
Route::get('/admin/manage-banking', 'admin\BankingController@ListBankingServices');
Route::get('/admin/add-banking', 'admin\BankingController@BankingServiceForm');
Route::get('/admin/add-banking/{id}', 'admin\BankingController@BankingServiceForm');
Route::post('/admin/banking-save-update', 'admin\BankingController@SaveUpdate');
Route::post('/admin/update-publish-status-banking-service', 'admin\BankingController@UpdateBankingServiceStatus');
Route::post('/admin/delete-banking-service', 'admin\BankingController@DeleteBankingService');



/*ListingController*/
Route::get('/admin/manage-listing', 'admin\ListingController@ListAll');
Route::get('/admin/add-listing', 'admin\ListingController@ListingForm');
Route::get('/admin/add-listing/{id}', 'admin\ListingController@ListingForm');
Route::post('/admin/listing-save-update', 'admin\ListingController@SaveUpdate');
Route::post('/admin/update-publish-status-list', 'admin\ListingController@UpdateListingStatus');
Route::post('/admin/update-feature-status-list', 'admin\ListingController@UpdateListingFeatureStatus');
Route::post('/admin/delete-listing', 'admin\ListingController@DeleteListing');


/*PortfolioController*/
Route::get('/admin/manage-portfolio', 'admin\PortfolioController@ListPortfolio');
Route::get('/admin/add-portfolio', 'admin\PortfolioController@PortfolioForm');
Route::get('/admin/add-portfolio/{id}', 'admin\PortfolioController@PortfolioForm');
Route::post('/admin/portfolio-save-update', 'admin\PortfolioController@SaveUpdate');
Route::post('/admin/update-publish-status-portfolio', 'admin\PortfolioController@UpdatePortfolioStatus');
Route::post('/admin/update-feature-status-portfolio', 'admin\PortfolioController@UpdatePortfolioFeatureStatus');
Route::post('/admin/delete-portfolio', 'admin\PortfolioController@DeletePortfolio');
Route::post('/admin/delete-iterineries', 'admin\PortfolioController@DeleteIterineries');
Route::post('/admin/delete-gallery-image', 'admin\PortfolioController@DeleteGalleryImage');


/*NewsController*/
Route::get('/admin/manage-news', 'admin\NewsController@ListNews');
Route::get('/admin/add-news', 'admin\NewsController@NewsForm');
Route::get('/admin/add-news/{id}', 'admin\NewsController@NewsForm');
Route::post('/admin/news-save-update', 'admin\NewsController@SaveUpdate');
Route::post('/admin/update-publish-status-news', 'admin\NewsController@UpdateNewsStatus');
Route::post('/admin/update-feature-status-news', 'admin\NewsController@UpdateNewsFeatureStatus');
Route::post('/admin/delete-news', 'admin\NewsController@DeleteNews');
Route::post('/admin/delete-iterineries', 'admin\NewsController@DeleteIterineries');
Route::post('/admin/delete-gallery-image', 'admin\NewsController@DeleteGalleryImage');

/*TravelGuideController*/
Route::get('/admin/manage-travelguide', 'admin\TravelGuideController@ListTravelGuide');
Route::get('/admin/add-travelguide', 'admin\TravelGuideController@TravelGuideForm');
Route::get('/admin/add-travelguide/{id}', 'admin\TravelGuideController@TravelGuideForm');
Route::post('/admin/travelguide-save-update', 'admin\TravelGuideController@SaveUpdate');
Route::post('/admin/update-publish-status-travelguide', 'admin\TravelGuideController@UpdateTravelGuideStatus');
Route::post('/admin/update-feature-status-travelguide', 'admin\TravelGuideController@UpdateTravelGuideFeatureStatus');
Route::post('/admin/delete-travelguide', 'admin\TravelGuideController@DeleteTravelGuide');
Route::post('/admin/delete-travel-image', 'admin\TravelGuideController@DeleteGalleryImage');

/*VisaController*/
Route::get('/admin/manage-visa-info', 'admin\VisaController@ListVisa');
Route::get('/admin/add-visa-info', 'admin\VisaController@VisaForm');
Route::get('/admin/add-visa-info/{id}', 'admin\VisaController@VisaForm');
Route::post('/admin/visa-save-update', 'admin\VisaController@SaveUpdate');
Route::post('/admin/delete-visa-info', 'admin\VisaController@DeleteVisa');


/*Contact Controller */
Route::get('/admin/contact-list', 'admin\ContactController@index');
Route::post('/admin/delete-contact', 'admin\ContactController@DeleteContact');


/*TeamController*/
Route::get('/admin/team-list', 'admin\TeamController@ListTeam');
Route::get('/admin/add-team', 'admin\TeamController@TeamForm');
Route::get('/admin/add-team/{id}', 'admin\TeamController@TeamForm');
Route::post('/admin/delete-team', 'admin\TeamController@DeleteTeam');
Route::post('/admin/delete-practice-area', 'admin\TeamController@DeletePracticeArea');
Route::post('/admin/team/saveupdate', 'admin\TeamController@SaveUpdate');
Route::get('/admin/edit-team/{id}', 'admin\TeamController@UpdateTeam');

/*FaqController*/
Route::get('/admin/add-faq', 'admin\FaqController@FaqForm');
Route::post('/admin/faq/saveupdate', 'admin\FaqController@SaveUpdate');
Route::post('/admin/delete-faq', 'admin\FaqController@DeleteFaq');

/*GalleryCotroller*/
Route::get('/admin/gallery', 'admin\GalleryController@GalleryForm');
Route::post('/admin/gallery/saveupdate', 'admin\GalleryController@SaveUpdate');
Route::post('/admin/delete-gallery', 'admin\GalleryController@DeleteGallery');

/*ManagementController*/
Route::get('/admin/management-list', 'admin\ManagementController@ListManagement');
Route::get('/admin/add-management', 'admin\ManagementController@ManagementForm');
Route::get('/admin/add-management/{id}', 'admin\ManagementController@ManagementForm');
Route::post('/admin/delete-management', 'admin\ManagementController@DeleteManagement');
Route::post('/admin/delete-practice-area-management', 'admin\ManagementController@DeletePracticeArea');
Route::post('/admin/management/saveupdate', 'admin\ManagementController@SaveUpdate');
Route::get('/admin/edit-management/{id}', 'admin\ManagementController@UpdateManagement');



/*Frontend*/
Route::get('/', 'PageController@index');
Route::get('/home', 'PageController@index');
Route::post('/contact-submit', 'PageController@ContactSubmit');
Route::get('/about', 'PageController@about');
Route::get('/our-team', 'PageController@OurTeam');
Route::get('/team-detail/{id}', 'PageController@OurTeamDetail');
Route::get('/management', 'PageController@OurManagement');
Route::get('/management-detail/{id}', 'PageController@OurManagementDetail');
Route::get('/contact-us', 'PageController@Contact');
Route::get('/portfolio', 'PageController@Portfolio');
Route::get('/portfolio-detail/{id}', 'PageController@PortfolioDetail');
Route::get('/faq', 'PageController@Faq');
Route::get('/news', 'PageController@News');
Route::get('/news-detail/{id}', 'PageController@NewsDetail');
Route::get('/gallery', 'PageController@Gallery');
Route::get('/domestic', 'PageController@GetService');
Route::get('/international', 'PageController@GetService');
Route::get('/contract', 'PageController@GetService');
Route::get('/banking', 'PageController@GetService');


Route::get('/domestic-detail/{name}/{id}', 'PageController@GetServiceDetail');
Route::get('/international-detail/{name}/{id}', 'PageController@GetServiceDetail');
Route::get('/contract-detail/{name}/{id}', 'PageController@GetServiceDetail');
Route::get('/banking-detail/{name}/{id}', 'PageController@GetServiceDetail');
