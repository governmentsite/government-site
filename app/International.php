<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class International extends Model
{

    public function FindAll()
    {
        $data = DB::table('tbl_international')->orderby('ID','ASC')->get();
        return $data;
    }

    public function getByID($id)
    {
        $data = DB::table('tbl_international')->where('ID', $id)->get();
        return $data;
    }



    public function SaveUpdate($data,$id=null)
    {
        if($id==""){

            DB::table('tbl_international')->insert($data);

        }else{

            DB::table('tbl_international')->where('ID',$id)->update($data);

        }
    }


}
