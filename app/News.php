<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class News extends Model
{

    public function FindAll()
    {
        $data = DB::table('tbl_news')->orderby('ID','ASC')->get();
        return $data;
    }

    public function getCategoryAndService()
    {
        $data = DB::table('tbl_news')->select('category')->distinct()->orderby('ID','ASC')->get();
        return $data;
    }




    public function getByID($id)
    {
        $data = DB::table('tbl_news')->where('ID', $id)->get();
        return $data;
    }



    public function SaveUpdate($data,$id=null)
    {
        if($id==""){
            DB::table('tbl_news')->insert($data);

        }else{
            DB::table('tbl_news')->where('ID',$id)->update($data);

        }

        redirect('admin/manage-news');


    }


}
