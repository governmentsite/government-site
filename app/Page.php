<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Page extends Model
{

    public function FindAll()
    {
        $data = DB::table('tbl_page')->orderby('ID','ASC')->get();
        return $data;
    }

    public function getByID($id)
    {
        $data = DB::table('tbl_page')->where('ID', $id)->get();
        return $data;
    }



    public function SaveUpdate($data,$id=null)
    {
        if($id==""){
            DB::table('tbl_page')->insert($data);

        }else{
            DB::table('tbl_page')->where('ID',$id)->update($data);

        }
    }


}
