-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2017 at 03:26 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo_lawyer`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `ID` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `admin_image` longtext NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `role` enum('0','1') NOT NULL DEFAULT '0',
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`ID`, `username`, `password`, `admin_image`, `full_name`, `role`, `status`, `created_date`, `updated_date`) VALUES
(1, 'admin', 'admin', '', '', '0', '1', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_banking`
--

CREATE TABLE `tbl_banking` (
  `ID` int(11) NOT NULL,
  `banking_title` varchar(225) NOT NULL,
  `banking_description` longtext NOT NULL,
  `banking_image` longtext NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_banking`
--

INSERT INTO `tbl_banking` (`ID`, `banking_title`, `banking_description`, `banking_image`, `status`, `created_date`) VALUES
(1, 'Banking', '<p>About Legal Research Associates</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ac malesuada ante. Curabitur lacinia diam tempus tempor consectet Sed vitae dignissim purus, eget aliquam libero. Duis et arcu a erat venenatis ornare eget nec urna. Nulla volutpat luctus venen Aliquam tellus dui, luctus nec ornare at, aliquet ac nulla. Quisque vitae feugiat eros. Pellentesque tempus tortor nec tellus el ifend, id dictum nibh volutpat.</p><p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit condimentum odio. Suspendisse lacinia, risus et porta dignissim, elit tellus iaculis tellus, eget efficitur elit magna eu orci. Phasellus tempor consectetur magna, at efficit est malesuada ac. Phasellus non ipsum diam. Suspendisse potenti.</p><p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curael ullam Nullam port id efficitur condimentum, dui nisl ullamcorper diam, at molestie nulla erat egestas nisi. Ut quis ex et ma gna egestas tempor Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p><p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit c</p>', '1503916485_banner2.jpg', '0', '2017-08-28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE `tbl_contact` (
  `ID` int(11) NOT NULL,
  `contact_name` varchar(50) NOT NULL,
  `contact_email` varchar(100) NOT NULL,
  `contact_phone` varchar(25) NOT NULL,
  `contact_message` longtext NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact`
--

INSERT INTO `tbl_contact` (`ID`, `contact_name`, `contact_email`, `contact_phone`, `contact_message`, `created_date`) VALUES
(1, 'Nathaniel Beard', 'dady@yahoo.com', '+255-38-3534252', 'Consequatur Ut architecto numquam aut veniam in voluptas aliquip fugiat autem qui sint officiis dolore rerum tenetur', '2017-10-24');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contract`
--

CREATE TABLE `tbl_contract` (
  `ID` int(11) NOT NULL,
  `contract_title` varchar(225) NOT NULL,
  `contract_description` longtext NOT NULL,
  `contract_image` longtext NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contract`
--

INSERT INTO `tbl_contract` (`ID`, `contract_title`, `contract_description`, `contract_image`, `status`, `created_date`) VALUES
(1, 'Contract', '<p>About Legal Research Associates</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ac malesuada ante. Curabitur lacinia diam tempus tempor consectet Sed vitae dignissim purus, eget aliquam libero. Duis et arcu a erat venenatis ornare eget nec urna. Nulla volutpat luctus venen Aliquam tellus dui, luctus nec ornare at, aliquet ac nulla. Quisque vitae feugiat eros. Pellentesque tempus tortor nec tellus el ifend, id dictum nibh volutpat.</p><p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit condimentum odio. Suspendisse lacinia, risus et porta dignissim, elit tellus iaculis tellus, eget efficitur elit magna eu orci. Phasellus tempor consectetur magna, at efficit est malesuada ac. Phasellus non ipsum diam. Suspendisse potenti.</p><p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curael ullam Nullam port id efficitur condimentum, dui nisl ullamcorper diam, at molestie nulla erat egestas nisi. Ut quis ex et ma gna egestas tempor Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p><p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit c</p>', '1503916950_banner1.jpg', '0', '2017-08-28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_domestic`
--

CREATE TABLE `tbl_domestic` (
  `ID` int(11) NOT NULL,
  `domestic_title` varchar(225) NOT NULL,
  `domestic_description` longtext NOT NULL,
  `domestic_image` longtext NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_domestic`
--

INSERT INTO `tbl_domestic` (`ID`, `domestic_title`, `domestic_description`, `domestic_image`, `status`, `created_date`) VALUES
(1, 'Domestic', '<p>About Legal Research Associates</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ac malesuada ante. Curabitur lacinia diam tempus tempor consectet Sed vitae dignissim purus, eget aliquam libero. Duis et arcu a erat venenatis ornare eget nec urna. Nulla volutpat luctus venen Aliquam tellus dui, luctus nec ornare at, aliquet ac nulla. Quisque vitae feugiat eros. Pellentesque tempus tortor nec tellus el ifend, id dictum nibh volutpat.</p><p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit condimentum odio. Suspendisse lacinia, risus et porta dignissim, elit tellus iaculis tellus, eget efficitur elit magna eu orci. Phasellus tempor consectetur magna, at efficit est malesuada ac. Phasellus non ipsum diam. Suspendisse potenti.</p><p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curael ullam Nullam port id efficitur condimentum, dui nisl ullamcorper diam, at molestie nulla erat egestas nisi. Ut quis ex et ma gna egestas tempor Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p><p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit c</p>', '1503915036_banner1.jpg', '0', '2017-09-03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faq`
--

CREATE TABLE `tbl_faq` (
  `ID` int(11) NOT NULL,
  `question` longtext NOT NULL,
  `answer` longtext NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_faq`
--

INSERT INTO `tbl_faq` (`ID`, `question`, `answer`, `created_date`) VALUES
(7, 'Non sint labore et qui blanditiis et dolores eu dolore in nobis', 'Ad velit perspiciatis voluptatibus cillum aut fugiat dignissimos odit earum est impedit vel debitis', '2017-08-28'),
(8, 'Ea natus suscipit duis ex tempora provident et quam dolores qui dolores nulla est sed porro', 'Sed eum dolore reprehenderit sunt eu nostrud dignissimos et', '2017-08-28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

CREATE TABLE `tbl_gallery` (
  `ID` int(11) NOT NULL,
  `image` longtext NOT NULL,
  `title` varchar(225) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`ID`, `image`, `title`, `created_date`) VALUES
(7, '1503947689_lawyer1a.jpg', 'Ut do veritatis do dolor aliquip qui elit qui magna hic', '2017-08-28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_international`
--

CREATE TABLE `tbl_international` (
  `ID` int(11) NOT NULL,
  `international_title` varchar(225) NOT NULL,
  `international_description` longtext NOT NULL,
  `international_image` longtext NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_international`
--

INSERT INTO `tbl_international` (`ID`, `international_title`, `international_description`, `international_image`, `status`, `created_date`) VALUES
(1, 'International', '<p>About Legal Research Associates</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ac malesuada ante. Curabitur lacinia diam tempus tempor consectet Sed vitae dignissim purus, eget aliquam libero. Duis et arcu a erat venenatis ornare eget nec urna. Nulla volutpat luctus venen Aliquam tellus dui, luctus nec ornare at, aliquet ac nulla. Quisque vitae feugiat eros. Pellentesque tempus tortor nec tellus el ifend, id dictum nibh volutpat.</p><p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit condimentum odio. Suspendisse lacinia, risus et porta dignissim, elit tellus iaculis tellus, eget efficitur elit magna eu orci. Phasellus tempor consectetur magna, at efficit est malesuada ac. Phasellus non ipsum diam. Suspendisse potenti.</p><p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curael ullam Nullam port id efficitur condimentum, dui nisl ullamcorper diam, at molestie nulla erat egestas nisi. Ut quis ex et ma gna egestas tempor Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p><p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit c</p>', '1503915940_banner2.jpg', '0', '2017-08-28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_listing`
--

CREATE TABLE `tbl_listing` (
  `ID` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `country` varchar(225) NOT NULL,
  `region` varchar(225) NOT NULL,
  `category` varchar(225) NOT NULL,
  `created_date` date NOT NULL,
  `status` enum('0','1') NOT NULL,
  `is_featured` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_listing`
--

INSERT INTO `tbl_listing` (`ID`, `title`, `country`, `region`, `category`, `created_date`, `status`, `is_featured`) VALUES
(3, 'Quia laboriosam dolores in recusandae Duis duis rem non', 'Nepal', 'Nepal Region', 'Nepal Category', '2017-08-09', '0', '0'),
(4, 'Qui perspiciatis voluptatem aut quos non placeat voluptatem Harum repudiandae earum inventore velit porro anim expedita dolore incididunt', 'Bhutan', 'Bhutan Region', 'Bhutan Category', '2017-08-09', '0', '1'),
(5, 'Culpa ut nisi ad error commodi exercitationem praesentium libero corporis quisquam ex', 'India', 'India Region', 'India Category', '2017-08-09', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_management`
--

CREATE TABLE `tbl_management` (
  `ID` int(11) NOT NULL,
  `member_name` varchar(225) NOT NULL,
  `member_email` varchar(225) NOT NULL,
  `member_phone` varchar(225) NOT NULL,
  `member_location` varchar(225) NOT NULL,
  `member_category` varchar(225) NOT NULL,
  `member_biography` longtext NOT NULL,
  `member_image` longtext NOT NULL,
  `member_designation` varchar(225) NOT NULL,
  `member_description` longtext NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_management`
--

INSERT INTO `tbl_management` (`ID`, `member_name`, `member_email`, `member_phone`, `member_location`, `member_category`, `member_biography`, `member_image`, `member_designation`, `member_description`, `status`, `created_date`) VALUES
(1, 'Freya Graves', 'fope@gmail.com', '+175-31-5192903', 'Est laudantium assumenda quae eos dolores autem ullamco iste aut minus hic alias dolorem fugiat perspiciatis mollitia', 'Officiis et sequi obcaecati quae quidem earum sit voluptatum est adipisicing commodi quaerat error consequatur Qui sunt dolorem quae hic', '<p>Aut dolores cupidatat incidunt est est et sed quia recusandae Suscipit obcaecati dolor nostrum consequatur qui similique dicta nullaAut dolores cupidatat incidunt est est et sed quia recusandae Suscipit obcaecati dolor nostrum consequatur qui similique dicta nullaAut dolores cupidatat incidunt est est et sed quia recusandae Suscipit obcaecati dolor nostrum consequatur qui similique dicta nullaAut dolores cupidatat incidunt est est et sed quia recusandae Suscipit obcaecati dolor nostrum consequatur qui similique dicta nullaAut dolores cupidatat incidunt est est et sed quia recusandae Suscipit obcaecati dolor nostrum consequatur qui similique dicta nullaAut dolores cupidatat incidunt est est et sed quia recusandae Suscipit obcaecati dolor nostrum consequatur qui similique dicta nullaAut dolores cupidatat incidunt est est et sed quia recusandae Suscipit obcaecati dolor nostrum consequatur qui similique dicta nullaAut dolores cupidatat incidunt est est et sed quia recusandae Suscipit obcaecati dolor nostrum consequatur qui similique dicta nullaAut dolores cupidatat incidunt est est et sed quia recusandae Suscipit obcaecati dolor nostrum consequatur qui similique dicta nullaAut dolores cupidatat incidunt est est et sed quia recusandae Suscipit obcaecati dolor nostrum consequatur qui similique dicta nullaAut dolores cupidatat incidunt est est et sed quia recusandae Suscipit obcaecati dolor nostrum consequatur qui similique dicta nullaAut dolores cupidatat incidunt est est et sed quia recusandae Suscipit obcaecati dolor nostrum consequatur qui similique dicta nullaAut dolores cupidatat incidunt est est et sed quia recusandae Suscipit obcaecati dolor nostrum consequatur qui similique dicta nulla</p>', '1504428099_download.png', 'Ea dolorem aliquid iste voluptas et aperiam quis delectus in voluptates', 'Sit ex sed praesentium consequatur Ex enim sint voluptatem excepteur qui sit saepe sequi autem reprehenderit', '0', '2017-09-03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE `tbl_news` (
  `ID` int(11) NOT NULL,
  `news_title` varchar(225) NOT NULL,
  `news_description` longtext NOT NULL,
  `news_image` longtext NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_news`
--

INSERT INTO `tbl_news` (`ID`, `news_title`, `news_description`, `news_image`, `status`, `created_date`) VALUES
(1, 'Officiis labore porro ut porro totam qui sint consequatur id dolor magni', '<p>Obcaecati sit placeat qui explicabo Est eos quibusdam non quia non quis irure beatae voluptas iusto non dolore consequuntur consequaturObcaecati sit placeat qui explicabo Est eos quibusdam non quia non quis irure beatae voluptas iusto non dolore consequuntur consequaturObcaecati sit placeat qui explicabo Est eos quibusdam non quia non quis irure beatae voluptas iusto non dolore consequuntur consequaturObcaecati sit placeat qui explicabo Est eos quibusdam non quia non quis irure beatae voluptas iusto non dolore consequuntur consequaturObcaecati sit placeat qui explicabo Est eos quibusdam non quia non quis irure beatae voluptas iusto non dolore consequuntur consequaturObcaecati sit placeat qui explicabo Est eos quibusdam non quia non quis irure beatae voluptas iusto non dolore consequuntur consequaturObcaecati sit placeat qui explicabo Est eos quibusdam non quia non quis irure beatae voluptas iusto non dolore consequuntur consequaturObcaecati sit placeat qui explicabo Est eos quibusdam non quia non quis irure beatae voluptas iusto non dolore consequuntur consequaturObcaecati sit placeat qui explicabo Est eos quibusdam non quia non quis irure beatae voluptas iusto non dolore consequuntur consequaturObcaecati sit placeat qui explicabo Est eos quibusdam non quia non quis irure beatae voluptas iusto non dolore consequuntur consequaturObcaecati sit placeat qui explicabo Est eos quibusdam non quia non quis irure beatae voluptas iusto non dolore consequuntur consequaturObcaecati sit placeat qui explicabo Est eos quibusdam non quia non quis irure beatae voluptas iusto non dolore consequuntur consequatur</p>', '1503944219_banner1.jpg', '0', '2017-08-21'),
(2, 'Eius iusto voluptatem rerum alias aut non et sequi et', '<p>Adipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna aut</p>', '1503944219_banner1.jpg', '0', '2017-08-21'),
(3, 'Legal Issues Regarding Paternity', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ac malesuada ante. Curabitur lacinia diam tempus tempor consectet Sed vitae dignissim purus, eget aliquam libero. Duis et arcu a erat venenatis ornare eget nec urna. Nulla volutpat luctus venen Aliquam tellus dui, luctus nec ornare at, aliquet ac nulla. Quisque vitae feugiat eros. Pellentesque tempus tortor nec tellus el ifend, id dictum nibh volutpat.</p><p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit condimentum odio. Suspendisse lacinia, risus et porta dignissim, elit tellus iaculis tellus, eget efficitur elit magna eu orci. Phasellus tempor consectetur magna, at efficit est malesuada ac. Phasellus non ipsum diam. Suspendisse potenti.</p><p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curael ullam Nullam port id efficitur condimentum, dui nisl ullamcorper diam, at molestie nulla erat egestas nisi. Ut quis ex et ma gna egestas tempor Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p><p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit condimentum odio. Suspendisse lacinia risus et porta dignissim, elit tellus iaculis tellus, eget efficitur elit magna eu orci. Phasellus tempor consesacte magna, at efficit est malesuada ac. Phasellus non ipsum diam. Suspendisse potenti.</p>', '1503944219_banner1.jpg', '0', '2017-08-28'),
(4, 'Aut odio et esse enim rerum autem', '<p>Aut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autemAut odio et esse enim rerum autem</p>', '1504428145_6760135001_58b1c5c5f0_b.jpg', '1', '2017-09-03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_page`
--

CREATE TABLE `tbl_page` (
  `ID` int(11) NOT NULL,
  `page_title` varchar(225) NOT NULL,
  `page_description` longtext NOT NULL,
  `page_image` longtext NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_page`
--

INSERT INTO `tbl_page` (`ID`, `page_title`, `page_description`, `page_image`, `status`, `created_date`) VALUES
(1, 'Domestic', '<p>About Legal Research Associates</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ac malesuada ante. Curabitur lacinia diam tempus tempor consectet Sed vitae dignissim purus, eget aliquam libero. Duis et arcu a erat venenatis ornare eget nec urna. Nulla volutpat luctus venen Aliquam tellus dui, luctus nec ornare at, aliquet ac nulla. Quisque vitae feugiat eros. Pellentesque tempus tortor nec tellus el ifend, id dictum nibh volutpat.</p><p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit condimentum odio. Suspendisse lacinia, risus et porta dignissim, elit tellus iaculis tellus, eget efficitur elit magna eu orci. Phasellus tempor consectetur magna, at efficit est malesuada ac. Phasellus non ipsum diam. Suspendisse potenti.</p><p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curael ullam Nullam port id efficitur condimentum, dui nisl ullamcorper diam, at molestie nulla erat egestas nisi. Ut quis ex et ma gna egestas tempor Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p><p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit c</p>', '1504368638_21125509_1559502370777281_2409863897465798383_o.jpg', '0', '2017-09-03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_portfolio`
--

CREATE TABLE `tbl_portfolio` (
  `ID` int(11) NOT NULL,
  `portfolio_title` varchar(225) NOT NULL,
  `portfolio_description` longtext NOT NULL,
  `category` varchar(225) NOT NULL,
  `portfolio_image` longtext NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_portfolio`
--

INSERT INTO `tbl_portfolio` (`ID`, `portfolio_title`, `portfolio_description`, `category`, `portfolio_image`, `status`, `created_date`) VALUES
(2, 'Eius iusto voluptatem rerum alias aut non et sequi et', '<p>Adipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna autAdipisci ea laboris dignissimos non enim aperiam est sed error magna aut</p>', 'Case', '1504427744_21192901_1472090062874808_531793602492507930_n.jpg', '0', '2017-09-03'),
(3, 'Legal Issues Regarding Paternity', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ac malesuada ante. Curabitur lacinia diam tempus tempor consectet Sed vitae dignissim purus, eget aliquam libero. Duis et arcu a erat venenatis ornare eget nec urna. Nulla volutpat luctus venen Aliquam tellus dui, luctus nec ornare at, aliquet ac nulla. Quisque vitae feugiat eros. Pellentesque tempus tortor nec tellus el ifend, id dictum nibh volutpat.</p><p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit condimentum odio. Suspendisse lacinia, risus et porta dignissim, elit tellus iaculis tellus, eget efficitur elit magna eu orci. Phasellus tempor consectetur magna, at efficit est malesuada ac. Phasellus non ipsum diam. Suspendisse potenti.</p><p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curael ullam Nullam port id efficitur condimentum, dui nisl ullamcorper diam, at molestie nulla erat egestas nisi. Ut quis ex et ma gna egestas tempor Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p><p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit condimentum odio. Suspendisse lacinia risus et porta dignissim, elit tellus iaculis tellus, eget efficitur elit magna eu orci. Phasellus tempor consesacte magna, at efficit est malesuada ac. Phasellus non ipsum diam. Suspendisse potenti.</p>', 'Case', '1503902798_banner2.jpg', '0', '2017-08-28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_practice_area`
--

CREATE TABLE `tbl_practice_area` (
  `ID` int(11) NOT NULL,
  `practice_area_title` text NOT NULL,
  `practice_area_description` longtext NOT NULL,
  `member_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_practice_area`
--

INSERT INTO `tbl_practice_area` (`ID`, `practice_area_title`, `practice_area_description`, `member_id`) VALUES
(23, 'Voluptate qui fugiat voluptas assumenda repudiandae delectus rerum', 'Dolorum est anim nobis dolores quis repellendus Possimus deleniti ducimus laborum Eos tempora sit', 2),
(36, 'Proident mollit adipisci proident nostrud aliquip', 'Fuga Ullam esse aut ex proident qui', 1),
(37, 'Voluptatem Excepteur obcaecati ut et iure sit dolorem mollit dolores nihil eu magna irure quae esse', 'Itaque et sunt quam non laudantium nobis voluptatem quasi aut ab pariatur Fugit iusto voluptas voluptates recusandae Aut non et', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_practice_area_management`
--

CREATE TABLE `tbl_practice_area_management` (
  `ID` int(11) NOT NULL,
  `practice_area_title` text NOT NULL,
  `practice_area_description` longtext NOT NULL,
  `member_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_practice_area_management`
--

INSERT INTO `tbl_practice_area_management` (`ID`, `practice_area_title`, `practice_area_description`, `member_id`) VALUES
(23, 'Voluptate qui fugiat voluptas assumenda repudiandae delectus rerum', 'Dolorum est anim nobis dolores quis repellendus Possimus deleniti ducimus laborum Eos tempora sit', 2),
(37, 'Aperiam facere accusantium officia sed numquam mollitia nulla in eius rem labore et quia sint libero', 'Sed repellendus Ducimus nemo pariatur Occaecat dolore sint enim ipsa officia culpa minim itaque quasi id', 1),
(38, 'Enim est in est omnis sint qui commodo eu accusantium animi qui do explicabo Unde architecto', 'Veritatis dolorem unde molestiae rerum laboriosam nesciunt non laborum Nulla exercitation fugit velit ea adipisicing voluptate voluptates quae velit', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_team`
--

CREATE TABLE `tbl_team` (
  `ID` int(11) NOT NULL,
  `member_name` varchar(225) NOT NULL,
  `member_email` varchar(225) NOT NULL,
  `member_phone` varchar(225) NOT NULL,
  `member_location` varchar(225) NOT NULL,
  `member_category` varchar(225) NOT NULL,
  `member_biography` longtext NOT NULL,
  `member_image` longtext NOT NULL,
  `member_designation` varchar(225) NOT NULL,
  `member_description` longtext NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_team`
--

INSERT INTO `tbl_team` (`ID`, `member_name`, `member_email`, `member_phone`, `member_location`, `member_category`, `member_biography`, `member_image`, `member_designation`, `member_description`, `status`, `created_date`) VALUES
(2, 'Hillary Hardin', 'sygeto@hotmail.com', '+889-62-7030444', 'Laboris voluptates fugiat minus anim veniam iste consectetur optio ut accusantium inventore', 'Omnis ulla', '<p>Necessitatibus esse quod qui et rerum id est enim nihil aut sint ipsum non irure sint sint commodi quiNecessitatibus esse quod qui et rerum id est enim nihil aut sint ipsum non irure sint sint commodi quiNecessitatibus esse quod qui et rerum id est enim nihil aut sint ipsum non irure sint sint commodi quiNecessitatibus esse quod qui et rerum id est enim nihil aut sint ipsum non irure sint sint commodi quiNecessitatibus esse quod qui et rerum id est enim nihil aut sint ipsum non irure sint sint commodi quiNecessitatibus esse quod qui et rerum id est enim nihil aut sint ipsum non irure sint sint commodi quiNecessitatibus esse quod qui et rerum id est enim nihil aut sint ipsum non irure sint sint commodi quiNecessitatibus esse quod qui et rerum id est enim nihil aut sint ipsum non irure sint sint commodi quiNecessitatibus esse quod qui et rerum id est enim nihil aut sint ipsum non irure sint sint commodi quiNecessitatibus esse quod qui et rerum id est enim nihil aut sint ipsum non irure sint sint commodi quiNecessitatibus esse quod qui et rerum id est enim nihil aut sint ipsum non irure sint sint commodi quiNecessitatibus esse quod qui et rerum id est enim nihil aut sint ipsum non irure sint sint commodi quiNecessitatibus esse quod qui et rerum id est enim nihil aut sint ipsum non irure sint sint commodi qui</p>', '1503911332_lawyer1.jpg', 'Dolores illum a sed anim sed aut sunt exercitationem velit culpa voluptas fuga Incidunt', 'Sint dicta quia assumenda voluptatem temporibus incidunt molestiae', '0', '2017-08-28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_banking`
--
ALTER TABLE `tbl_banking`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_contract`
--
ALTER TABLE `tbl_contract`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_domestic`
--
ALTER TABLE `tbl_domestic`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_faq`
--
ALTER TABLE `tbl_faq`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_international`
--
ALTER TABLE `tbl_international`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_listing`
--
ALTER TABLE `tbl_listing`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_management`
--
ALTER TABLE `tbl_management`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_news`
--
ALTER TABLE `tbl_news`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_page`
--
ALTER TABLE `tbl_page`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_portfolio`
--
ALTER TABLE `tbl_portfolio`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_practice_area`
--
ALTER TABLE `tbl_practice_area`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_practice_area_management`
--
ALTER TABLE `tbl_practice_area_management`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_team`
--
ALTER TABLE `tbl_team`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_banking`
--
ALTER TABLE `tbl_banking`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_contract`
--
ALTER TABLE `tbl_contract`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_domestic`
--
ALTER TABLE `tbl_domestic`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_faq`
--
ALTER TABLE `tbl_faq`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_international`
--
ALTER TABLE `tbl_international`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_listing`
--
ALTER TABLE `tbl_listing`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_management`
--
ALTER TABLE `tbl_management`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_news`
--
ALTER TABLE `tbl_news`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_page`
--
ALTER TABLE `tbl_page`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_portfolio`
--
ALTER TABLE `tbl_portfolio`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_practice_area`
--
ALTER TABLE `tbl_practice_area`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tbl_practice_area_management`
--
ALTER TABLE `tbl_practice_area_management`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `tbl_team`
--
ALTER TABLE `tbl_team`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
