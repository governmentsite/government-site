@include('admin.includes.header')

<body>
@include('admin.includes.footer')
<section class="body">

	<div class="inner-wrapper">
		@include('admin.includes.nav')
			<section role="main" class="content-body">
				<header class="page-header">
					<h2>Manage Dashboard</h2>
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="{{url('admin/dashboard')}}"><i class="fa fa-home"></i></a></li>
							<li><span>Manage Dashboard</span></li>
							<li><span>Dashboard</span></li>
						</ol>
							<a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
					</div>
				</header>

			</section>
		</div>
	</section>
	

	

@include('admin.includes.footer')
</body>
</html>