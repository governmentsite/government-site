@include('admin.includes.header')
<body>
<section class="body">

	<div class="inner-wrapper">
		@include('admin.includes.nav')
		<section role="main" class="content-body">
				<header class="page-header">
					<h2>Manage Team</h2>					
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="#"><i class="fa fa-home"></i></a></li>
							<li><span>Manage Pages</span></li>
							<li><span>Our Team</span></li>
						</ol>
							<a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
					</div>
				</header>
				<form method="post" action="{{url('admin/faq/saveupdate')}}" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							</div>
							<h2 class="panel-title">Team Members</h2>
						</header>
						<div class="panel-body">

								@if($data!=='')
									@foreach($data['data'] as $value)
							<div class="row">
								<div class="col-sm-9">
									<div class="form-group">
										<label class="control-label">Question</label>
										<input type="text" name="question[]" value="{{ $value->question }}" class="form-control">
									</div>
									<div class="form-group">
										<label class="control-label">Answer</label>
										<textarea name="answer[]"  class="form-control">{{$value->answer }}</textarea>
									</div><br>
									<button type="button" class="removeImage btn btn-danger" onclick="deleteTeam({{$value->ID}})"><i class="fa fa-trash"></i> Delete Member</button><hr>
								</div>
							</div>
									@endforeach
								@endif
							<div class="row">
								<div class="col-sm-12">
									<button type="button" class="addNewImage btn btn-primary"><i class="fa fa-plus"></i> Add New Faq</button>
								</div>
							</div>
						</div>
					</section>
					<div class="addNew">
						<button class="" type="submit"><i class="fa fa-save"></i> Save</button>
					</div>
				</form>
			</section>
	</div>


</section>


@include('admin.includes.footer')
<script>
    $('.addNewImage').click(function() {
        debugger;
        $(this).before('<div class="row">' +
			'<div class="col-sm-9">' +
			'<div class="form-group">' +
			'<label class="control-label">Question</label>' +
			'<input type="text" name="question[]" class="form-control"></div>' +
            '<div class="form-group">' +
        	'<label class="control-label">Answer</label>' +
        	'<textarea  name="answer[]" class="form-control"></textarea>' +
        	'</div>' +
			'<br>' +
			'<button type="button" class="removeImage btn btn-danger"><i class="fa fa-trash"></i> Delete Faq</button><hr></div></div>');
        $('#whatever').append(structure);
    });
    $(document).on('click','.removeImage',function() {
        $(this).parent('div').parent('div').remove();
    });

    $('.image-input').change(function() {
        debugger;
        $(this).val($(this).val().replace("C:\\fakepath\\", ""));
    });

    function deleteTeam(id) {
        var id = id;
        if (confirm('Are you sure you want to delete this?')) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{url('admin/delete-faq')}}',
                method: 'post',
                data: {id: id},
                success: function () {
                    $(this).parent('div').remove();

                }
            })
        }
    }






</script>
</body>
</html>