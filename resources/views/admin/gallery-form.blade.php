@include('admin.includes.header')
@php($add = 1)
@if(isset($data)!=0)
	@php($add = 0)
@endif
<body>
<section class="body">

	<div class="inner-wrapper">
		@include('admin.includes.nav')
		<section role="main" class="content-body">
				<header class="page-header">
					<h2>Manage Gallery</h2>
					<div class="right-wrapper pull-right">
						<ol class="breadcrumbs">
							<li><a href="#"><i class="fa fa-home"></i></a></li>
							<li><span>Manage Gallery</span></li>
							<li><span>Our Gallery</span></li>
						</ol>
						<a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
					</div>
				</header>
				<form method="post" action="{{url('admin/gallery/saveupdate')}}" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<section class="panel">
						<header class="panel-heading">
							<div class="panel-actions">
								<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
							</div>
							<h2 class="panel-title">Gallery Images</h2>
						</header>
						<div class="panel-body">
							@if(!$add)
								@if($data!=='')
									@foreach($data['data'] as $value)
							<div class="row">
								<div class="col-sm-3">
									<img src="{{(!$add) ? url('public/uploads/gallery/'.$value->image) : '' }}" class="proImg" alt="Gallery Image">
									<div class="overlay">
										<label for="image-input" title="Upload Profile Pic"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label>
										<input type="file" name="image[]" value="{{(!$add) ? $value->image : '' }}"  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file" >
									</div>
								</div>
								<div class="col-sm-9">
									<div class="form-group">
										<label class="control-label">Title</label>
										<input type="text" name="title[]" value="{{(!$add) ? $value->title : '' }}" class="form-control">
									</div>
									<br>
									<button type="button" class="removeImage btn btn-danger" onclick="deleteGallery({{$value->ID}})"><i class="fa fa-trash"></i> Delete Image</button><hr>
								</div>
							</div>
									@endforeach
								@endif
							@endif
							<div class="row">
								<div class="col-sm-12">
									<button type="button" class="addNewImage btn btn-primary"><i class="fa fa-plus"></i> Add New Image</button>
								</div>
							</div>
						</div>
					</section>
					<div class="addNew">
						<button class="" type="submit"><i class="fa fa-save"></i> Save</button>
					</div>
				</form>
			</section>
	</div>


</section>


@include('admin.includes.footer')
<script>
    $('.addNewImage').click(function() {
        $(this).before('<div class="row">' +
			'<div class="col-sm-3">' +
			'<img src="http://lorempixel.com/200/200/nature/" class="proImg" alt="Gallery Image">' +
			'<div class="overlay">' +
			'<label for="image-input" title="Upload Profile Pic">' +
			'<i class="fa fa-camera" onclick="uploadImage(this)"></i>' +
			'</label>' +
			'<input type="file" name="image[]"  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" >' +
			'</div>' +
			'</div>' +
			'<div class="col-sm-9">' +
			'<div class="form-group">' +
			'<label class="control-label">Title</label>' +
			'<input type="text" name="title[]" class="form-control"></div>' +
			'<br>' +
			'<button type="button" class="removeImage btn btn-danger"><i class="fa fa-trash"></i> Delete Image</button><hr></div></div>');
        $('#whatever').append(structure);
    });
    $(document).on('click','.removeImage',function() {
        $(this).parent('div').parent('div').remove();
    });

    $('.image-input').change(function() {
        $(this).val($(this).val().replace("C:\\fakepath\\", ""));
    });

    function deleteGallery(id) {
        var id = id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{url('admin/delete-gallery')}}',
            method:'post',
            data:{id:id},
            success:function () {
                $(this).parent('div').remove();

            }
        })
    }






</script>
</body>
</html>