@include('admin.includes.header')
@php($add = 1)
@if($data!=null)
    @php($add = 0)
@endif
<body>
<section class="body">
    <div class="inner-wrapper">
        @include('admin.includes.nav')
        <section role="main" class="content-body">
            <header class="page-header">
                <h2>Manage Team</h2>
                <div class="right-wrapper pull-right">
                    <ol class="breadcrumbs">
                        <li><a href="{{url('/')}}"><i class="fa fa-home"></i></a></li>
                        <li><span>Manage Team</span></li>
                        <li><span>Business</span></li>
                    </ol>
                   	<a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
                </div>
            </header>
            <form method="post" action="{{url('/admin/management/saveupdate')}}" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="ID" value="{{ !$add ? $data[0]->ID : '' }}">
                <section class="panel">
                    <div class="row">
                        <div class="col-md-12">
                            <header class="panel-heading">
                                <div class="panel-actions">
                                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                </div>
                                <h2 class="panel-title">Basic Information</h2>
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Name</label>
                                            <input type="text" name="member_name" class="form-control"
                                                   value="{{ !$add ? $data[0]->member_name : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Position</label>
                                            <input type="text" name="member_designation" class="form-control"
                                                   value="{{ !$add ? $data[0]->member_designation : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Short Description</label>
                                            <textarea name="member_description" class="form-control" value="{{ !$add ? $data[0]->member_description : '' }}">{{ !$add ? $data[0]->member_description : '' }}</textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
                <section class="panel">
                    <div class="row">
                        <div class="col-md-12">
                            <header class="panel-heading">
                                <div class="panel-actions">
                                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                                </div>
                                <h2 class="panel-title">Listing Attributes</h2>
                            </header>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Category</label>
                                            <input type="text" name="member_category" class="form-control"
                                                   value="{{ !$add ? $data[0]->member_category : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Location</label>
                                            <input type="text" name="member_location"
                                                   value="{{ !$add ? $data[0]->member_location : '' }}"
                                                   class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Email</label>
                                            <input type="text" name="member_email"
                                                   value="{{ !$add ? $data[0]->member_email : '' }}"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Telephone</label>
                                            <input type="text" name="member_phone"
                                                   value="{{ !$add ? $data[0]->member_phone : '' }}" class="form-control">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        </div>
                        <h2 class="panel-title">Manage Practice Areas</h2>
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Practice Areas</label>
                                    @if(!$add)
                                        @if($practiceAreas!=0)
                                            @foreach($practiceAreas as $value)
                                                <div class="input-group">
                                                    <input type="text" name="practice_area_title[]" value="{{(!$add) ? $value->practice_area_title : '' }}" class="form-control" >
                                                    <br>
                                                    <textarea name="practice_area_description[]" value="{{(!$add) ? $value->practice_area_description : '' }}" class="form-control" >{{(!$add) ? $value->practice_area_description : '' }}</textarea>
                                                    <span class="input-group-btn">
														<button class="btn btn-danger removeItenieraries" onclick="deleteThis({{$value->ID}})" type="button"><i class="glyphicon glyphicon-trash"></i></button>
        											</span>
                                                </div>

                                            @endforeach
                                        @endif
                                    @else
                                        <input type="text" name="practice_area_title[]" placeholder="Practice Area Title"  class="form-control" >
                                    <br>
                                        <textarea name="practice_area_description[]" placeholder="Practice Area Description"  class="form-control" ></textarea>
                                    @endif
                                </div>
                                <div class="clonehere"></div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-success pull-right" id="clonebtn" onclick="cloneItenieraries()"><i class="glyphicon glyphicon-plus-sign"></i> </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        </div>
                        <h2 class="panel-title">Manage Contents</h2>
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">Biography</label>
                                    <textarea name="member_biography" class="form-control" id="editor1">{{ !$add ? $data[0]->member_biography : '' }}</textarea>
                                    <script>
                                        CKEDITOR.replace('editor1');
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        </div>
                        <h2 class="panel-title">Gallery Image</h2>
                    </header>
                    <div class="panel-body">
                        @if(!$add && $data[0]->member_image!=0)
                                <div class="row">
                                    <div class="col-sm-3">
                                        <img src="{{url('public/uploads/management/'.$data[0]->member_image)}}" class="proImg" alt="Gallery Image">
                                        <div class="overlay">
                                            <label for="image-input" title="Upload Profile Pic"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label>
                                            <input type="file"  name="member_image" value="{{(!$add && $data[0]->member_image!=="") ? $data[0]->member_image : '' }}"  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" >
                                        </div>
                                    </div>
                                </div>
                        @else
                            <div class="row">
                                <div class="col-sm-3">
                                    <img src="https://www.zinfi.com/wp-content/uploads/2016/11/dummy-img.png" class="proImg" alt="Page Image">
                                    <div class="overlay">
                                        <label for="image-input" title="Upload Page Image"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label>
                                        <input type="file"  name="member_image" value=""  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" >
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </section>
                <div class="addNew">
                    <button class="" type="submit"><i class="fa fa-save"></i> Save</button>
                </div>
            </form>
        </section>
    </div>
</section>

<script src="{{url('public/admin-assets/vendor/jquery/jquery.js')}}"></script>
<script src="{{url('public/admin-assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
<script src="{{url('public/admin-assets/vendor/jquery-cookie/jquery-cookie.js')}}"></script>
<script src="{{url('public/admin-assets/vendor/bootstrap/js/bootstrap.js')}}"></script>
<script src="{{url('public/admin-assets/vendor/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
<script src="{{url('public/admin-assets/vendor/nanoscroller/nanoscroller.js')}}"></script>
<script src="{{url('public/admin-assets/vendor/jquery-placeholder/jquery-placeholder.js')}}"></script>
<script src="{{url('public/admin-assets/vendor/select2/js/select2.js')}}"></script>
<script src="{{url('public/admin-assets/javascripts/theme.js')}}"></script>
<script src="{{url('public/admin-assets/javascripts/theme.custom.js')}}"></script>
<script src="{{url('public/admin-assets/javascripts/theme.init.js')}}"></script>
<script>
    $('.addNewImage').click(function () {
        $(this).before('<div class="row"><div class="col-sm-3"><img src="http://lorempixel.com/200/200/nature/" class="proImg" alt="Gallery Image"><div class="overlay"><label for="image-input" title="Upload Profile Pic"><i class="fa fa-camera" onclick="uploadImage(this)"></i></label><input type="file" name="travelguide_image[]"  onchange="readURL(this);" accept="image/*" class="form-control form-input Profile-input-file image-input" ></div></div><div class="col-sm-9"><div class="form-group"><label class="control-label">Image Title</label><input type="text" name="image_title[]" class="form-control"></div><div class="form-group"><label class="control-label">Image Description</label><input type="text" name="image_description[]" class="form-control"></div><br><button type="button" class="removeImage btn btn-danger"><i class="fa fa-trash"></i> Delete Image</button><hr></div></div>');
        $('#whatever').append(structure);
    });

    $(document).on('click', '.removeItenieraries', function () {
        $(this).closest('div').parent('div').remove();
    });

    function deleteThis(id) {
        var id = id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{url('admin/delete-practice-area-management')}}',
            method:'post',
            data:{id:id},
            success:function () {
               location.reload();

            }
        });

    }

    function cloneItenieraries() {
        $('.clonehere').append('<div class="form-group">'+
            '<label class="control-label">Practice Area</label>'+
            '<div class="input-group">'+
            '<input type="text" name="practice_area_title[]" value="" class="form-control" placeholder="Practice Area Title" >'+
                '<br>'+
            '<textarea  name="practice_area_description[]" value="" class="form-control" placeholder="Practice Area Description" ></textarea>'+
            '<span class="input-group-btn">'+
            '<button class="btn btn-danger removeItenieraries" type="button"><i class="glyphicon glyphicon-trash"></i></button>'+
            '</span>'+
            '</div>'+
            '</div>');
    }
    function deleteImage(id) {
        var id = id;
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{url('admin/delete-travel-image')}}',
            method: 'post',
            data: {id: id},
            success: function () {
                //location.reload();

            }
        });

    }


</script>
</body>

</html>
