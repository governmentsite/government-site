@php($pgName = 'FAQs | Legal Research Associats')
	@include('includes.top')

	<div class="lawyer-main-wrapper">
		@include('includes.header')
		<div class="lawyer-subheader">
			<div class="lawyer-subheader-image">
				<span class="lawyer-dark-transparent"></span>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1>FAQs</h1>
							<span>Frequently Asked Questions</span>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><a href="{{url('home')}}">Home </a></li>
								<li>FAQs</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lawyer-main-content">
			<div class="lawyer-main-section">
				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<div class="lawyer-fancy-title">
								<h2><span class="lawyer-color">FAQs</span></h2>
								<span>Frequently Asked Questions <small></small></span>
							</div>
								@if($data!=0)
								@foreach($data as $value)
									<div class="panel-group faq-accordion" id="{{$value->ID}}accordion" role="tablist" aria-multiselectable="true">
								<div class="panel panel-default">
									<div class="panel-heading active" role="tab" id="{{$value->ID}}Heading">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#{{$value->ID}}accordion" href="#{{$value->ID}}" aria-expanded="true" aria-controls="collapseOne">{{$value->question}}</a>
										</h4>
									</div>
									<div id="{{$value->ID}}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="{{$value->ID}}Heading">
										<div class="panel-body">
											{{$value->answer}}
										</div>
									</div>
								</div>

							</div>
								@endforeach
							@endif
						</div>
						<aside class="col-md-3">
							@include('includes.sidebar')
						</aside>
					</div>
				</div>
			</div>
		</div>
		@include('includes.footer')
		<div class="clearfix"></div>
	</div>
	@include('includes.btm')