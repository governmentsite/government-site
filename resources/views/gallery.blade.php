@php($pgName = 'Gallery| Legal Research Associats')
	@include('includes.top')

	<div class="lawyer-main-wrapper">
		@include('includes.header')
		<div class="lawyer-subheader">
			<div class="lawyer-subheader-image">
				<span class="lawyer-dark-transparent"></span>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1>Gallery</h1>
							<span>Picture Gallery</span>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><a href="{{url('home')}}">Home </a></li>
								<li>Gallery</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lawyer-main-content">
			<div class="lawyer-main-section">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="lawyer-gallery lawyer-modern-gallery">
								<ul class="row">
									<ul id="itemContainer">
									@if($data!=0)
										@foreach($data as $value)
									<li class="col-md-4">
										<div class="lawyer-gallery-wrap">
											<figure>
												<a href="#"><img src="{{url('public/uploads/gallery/'.$value->image)}}" alt=""></a>
												<figcaption>
													<a href="#"><i class="fa fa-arrows"></i></a> 
													<h6><a href="#">{{$value->title}}</a></h6>
												</figcaption>
											</figure>
										</div> 
									</li>
										@endforeach
										@endif
									</ul>
								</ul>
							</div>
						</div>
						<div class="lawyer-pagination">
							<div class="holder" style="text-align: center!important;"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('includes.footer')
		<div class="clearfix"></div>
	</div>
	@include('includes.btm')