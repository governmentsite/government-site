		<footer class="lawyer-footer-one" id="lawyer-footer">
			<div class="lawyer-footer-widget">
				<div class="container">
					<div class="row">
						<aside class="col-md-4 widget widget_links">
                            <div class="footer-widget-title"><h2>About Us</h2></div>
                            @php($about = \App\Http\Controllers\PageController::StaticAboutUS())
                            {{strip_tags(substr($about['about'][0]->page_description,0,282))}}
                        </aside>
						<aside class="col-md-4 widget widget_populer_posts">
                            <div class="footer-widget-title"><h2>Populer Posts</h2></div>
                            <ul>
								@php($news = \App\Http\Controllers\admin\NewsController::StaticNewsAsc())
								@if($news!=0)
									@foreach($news as $value)
                                <li>
                                    <figure><a href="{{url('news-detail/'.$value->ID)}}"><img src="{{url('public/uploads/newss/'.$value->news_image)}}" alt=""><i class="fa fa-angle-double-right"></i></a></figure>
                                    <section>
                                        <h6><a href="{{url('news-detail/'.$value->ID)}}">{{$value->news_title}}</a></h6>
                                        <time datetime="2008-02-14 20:00"><i class="fa fa-calendar-o"></i>{{$value->created_date}}</time>
                                    </section>
                                </li>
									@endforeach
								@endif
                            </ul>
                        </aside>
						<aside class="col-md-4 widget widget_newsletter">
                            <div class="footer-widget-title"><h2>Contact Us</h2></div>
                            <p>
								<i class="fa fa-fw fa-paper-plane-o"></i> Post Box 828 <br>
								<i class="fa fa-fw fa-map-marker"></i> Anamnagar, Kathmandu, Nepal<br>
								<i class="fa fa-fw fa-phone"></i> <a href="tel:+977014257991"> +977 01 4257991</a><br>
								<i class="fa fa-fw fa-print"></i> <a href="tel:+977 01 4252610"> +977 01 4252610</a><br>
								<i class="fa fa-fw fa-envelope"></i> <a href="mailto:info@lawyersinnepal.com">info@lawyersinnepal.com</a>
 							</p>
                        </aside>
						<div class="col-md-12">
							<div class="lawyer-copyright">
								<p><i class="fa fa-copyright"></i> 2017, All Right Reserved - Legal Research Associates. Powered by: <a href="http://www.karmatech.com.np" target="_blank">Karma Tech</a></p>
								<a class="lawyer-back-top lawyer-bgcolor" href="#"><i class="fa fa-angle-up"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>