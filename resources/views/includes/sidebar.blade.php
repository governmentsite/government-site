						<div class="lawyer-widget-heading"><h2>Services</h2></div>
							<div class="widget widget_cetagories">
								<ul>
									<li><figure><a href="{{url('domestic')}}" title="Domestic Consultation">Domestic Consultation <i class="icon-auction"></i></a></figure></li>
									<li><figure><a href="{{url('international')}}" title="International Consulation">International Consulation <i class="icon-libra"></i></a></figure></li>
									<li><figure><a href="{{url('banking')}}" title="Banking">Banking Services <i class="icon-money-bag"></i></a></figure></li>
									<li><figure><a href="{{url('contract')}}" title="Contracts">Contracts Services<i class="icon-diploma"></i></a></figure></li>
								</ul>
							</div>
							<div class="lawyer-widget-heading"><h2>Popular Posts</h2></div>
							<div class="widget widget_populer_posts">
								<ul>
									@php($news = \App\Http\Controllers\admin\NewsController::StaticNews())
									@if($news!=0)
									@foreach($news as $value)
									<li>
										<figure><a href="{{url('news-detail/'.$value->ID)}}"><img src="{{url('public/uploads/newss/'.$value->news_image)}}" alt=""><i class="fa fa-angle-double-right"></i></a></figure>
										<section>
											<h6><a href="{{url('news-detail/'.$value->ID)}}">{{$value->news_title}}</a></h6>
											<time datetime="2008-02-14 20:00"><i class="fa fa-calendar-o"></i>{{$value->created_date}}</time>
										</section>
									</li>
										@endforeach
										@endif
								</ul>
							</div>
							<div class="lawyer-widget-heading"><h2>Attorneys</h2></div>
							<div class="widget widget widget_attorney">
								@php($team = \App\Http\Controllers\admin\TeamController::StaticTeam())
								@if($team!=0)
									@foreach($team as $value)
								<div>
									<figure><a href="{{url('team-detail/'.$value->ID)}}"><img src="{{url('public/uploads/team/'.$value->member_image)}}" alt=""></a></figure>
									<div class="widget-attorney-text">
										<h6><a href="{{url('team-detail/'.$value->ID)}}">{{$value->member_name}}</a></h6>
										<span>{{$value->member_category}}</span>
									</div>
								</div>
									@endforeach
								@endif
								<div>
									<figure><a href="team-detail.php"><img src="{{url('public/assets/img/lawyer2.jpg')}}" alt=""></a></figure>
									<div class="widget-attorney-text">
										<h6><a href="team-detail.php">Umesh Pd. Sitaula</a></h6>
										<span>Lawyer Cetagory</span>
									</div>
								</div>
							</div>