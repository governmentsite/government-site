@php($pgName = 'Management | Legal Research Associates')
	@include('includes.top')

	<div class="lawyer-main-wrapper">
		@include('includes.header')
		<div class="lawyer-subheader">
			<div class="lawyer-subheader-image">
				<span class="lawyer-dark-transparent"></span>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1>About Management</h1>
							<span>Legal Research Associates</span>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><a href="{{url('home')}}">Home </a></li>
								<li>About</li>
								<li>Management</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lawyer-main-content">
			<div class="lawyer-main-section">
				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<div class="lawyer-attorney lawyer-attorney-classic">
								<ul class="row">
									<ul id="itemContainer">
									@if($data!=0)
										@foreach($data as $value)
									<li class="col-md-12">
										<figure><a href="{{url('management-detail/'.$value->ID)}}"><img src="{{url('public/uploads/management/'.$value->member_image)}}" alt=""><i class="fa fa-link"></i></a></figure>
										<div class="lawyer-attorney-classic-text">
											<h4>{{$value->member_name}}</h4>
											<ul class="lawyer-attorney-social">
												<li><a href="https://www.facebook.com/" class="icon-facebook2"></a></li>
												<li><a href="https://twitter.com/login" class="icon-social62"></a></li>
												<li><a href="https://pk.linkedin.com/" class="icon-social3"></a></li>
												<li><a href="https://plus.google.com/" class="icon-google-plus2"></a></li>
											</ul>
											<br>
											<span>{{$value->member_category}}</span>
											{{strip_tags(substr($value->member_description,0,230))}}
											<a href="{{url('management-detail/'.$value->ID)}}" class="lawyer-blog-read-btn">Read More <span></span></a>
										</div>
									</li>
										@endforeach
										@endif
									</ul>
								</ul>
							</div>
							<div class="lawyer-pagination">
								<div class="holder" style="text-align: center!important;"></div>
							</div>
						</div>
						<aside class="col-md-3">
							@include('includes.sidebar')
						</aside>
					</div>
				</div>
			</div>
		</div>
		@include('includes.footer')
		<div class="clearfix"></div>
	</div>
	@include('includes.btm')