@php($pgName = 'News Title | Legal Research Associats')
	@include('includes.top')
	<div class="lawyer-main-wrapper">
		@include('includes.header')
		<div class="lawyer-subheader">
			<div class="lawyer-subheader-image">
				<span class="lawyer-dark-transparent"></span>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1>News</h1>
							<span>{{$data[0]->news_title}}</span>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><a href="index.php">Home </a></li>
								<li>News</li>
								<li>{{$data[0]->news_title}}</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lawyer-main-content">
			<div class="lawyer-main-section">
				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<figure class="lawyer-figure-thumb"><img src="{{url('public/assets/img/about.jpg')}}" alt="">
								<figcaption>
									<div class="laywer-thumb-text">
										<h3>{{$data[0]->news_title}}</h3>
									</div>
								</figcaption>
							</figure>
							<div class="lawyer-rich-editor" style="word-wrap:break-word;">
							{{strip_tags($data[0]->news_description)}}
							</div>
						</div>
						<aside class="col-md-3">
							@include('includes.sidebar')
						</aside>
					</div>
				</div>
			</div>
		</div>
		@include('includes.footer')
		<div class="clearfix"></div>
	</div>
	@include('includes.btm')