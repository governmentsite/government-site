@php($pgName = 'News & Updates | Legal Research Associats')
	@include('includes.top')
	<div class="lawyer-main-wrapper">
		@include('includes.header')
		<div class="lawyer-subheader">
			<div class="lawyer-subheader-image">
				<span class="lawyer-dark-transparent"></span>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1>News</h1>
							<span>Latest News & Updates</span>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><a href="{{url('home')}}">Home </a></li>
								<li>News</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lawyer-main-content">
			<div class="lawyer-main-section">
				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<div class="lawyer-blog lawyer-classic-blog">
								<ul class="row">
									<ul id="itemContainer">
									@if($data!=0)
										@foreach($data as $value)
									<li class="col-md-6">
										<figure><a href="{{url('news-detail/'.$value->ID)}}"><img src="{{url('public/uploads/newss/'.$value->news_image)}}" alt=""><i class="fa fa-link"></i></a></figure>
										<div class="lawyer-classic-blog-text">
											<time datetime="2008-02-14 20:00">{{$value->created_date}}</time>
											<h4><a href="{{url('news-detail/'.$value->ID)}}">{{$value->news_title}}</a></h4>
											{{strip_tags(substr($value->news_description,0,69))}}
											<br>
											<a href="{{url('news-detail/'.$value->ID)}}" class="lawyer-readmore-btn">Read More</a>
										</div>
									</li>
										@endforeach
									@endif
									</ul>
								</ul>
							</div>
							<div class="lawyer-pagination">
								<div class="holder" style="text-align: center!important;"></div>
							</div>
						</div>
						<aside class="col-md-3">
							@include('includes.sidebar')
						</aside>
					</div>
				</div>
			</div>
		</div>
		@include('includes.footer')
		<div class="clearfix"></div>
	</div>
	@include('includes.btm')