@php($pgName = 'Portfolio Title | Legal Research Associats')
	@include('includes.top')

	<div class="lawyer-main-wrapper">
		@include('includes.header')
		<div class="lawyer-subheader">
			<div class="lawyer-subheader-image">
				<span class="lawyer-dark-transparent"></span>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1>{{$data[0]->portfolio_title}}</h1>
							<span>Vestibulum at lorem lacinia</span>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><a href="index.php">Home </a></li>
								<li>Portfolio</li>
								<li>{{$data[0]->portfolio_title}}</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lawyer-main-content">
			<div class="lawyer-main-section">
				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<figure class="lawyer-figure-thumb"><img src="{{url('public/uploads/potfolios/'.$data[0]->portfolio_image)}}" alt="">
								<figcaption>
									<div class="laywer-thumb-text">
										<h3>{{$data[0]->portfolio_title}}</h3>
									</div>
								</figcaption>
							</figure>
							<div class="lawyer-rich-editor">
							{{strip_tags($data[0]->portfolio_description)}}
							</div>
						</div>
						<aside class="col-md-3">
							@include('includes.sidebar')
						</aside>
					</div>
				</div>
			</div>
		</div>
		@include('includes.footer')
		<div class="clearfix"></div>
	</div>
	@include('includes.btm')