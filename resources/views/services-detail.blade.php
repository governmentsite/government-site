@php($pgName = 'Service Title | Legal Research Associats')
	@include('includes.top')
@php($title = $url."_title")
@php($description = $url."_description")
@php($image = $url."_image")
	<div class="lawyer-main-wrapper">
		@include('includes.header')
		<div class="lawyer-subheader">
			<div class="lawyer-subheader-image">
				<span class="lawyer-dark-transparent"></span>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1>Our Service</h1>
							<span>{{$data[0]->$title}}</span>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><a href="{{url('home')}}">Home </a></li>
								<li>Services</li>
								<li>{{$data[0]->$title}}</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lawyer-main-content">
			<div class="lawyer-main-section">
				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<figure class="lawyer-figure-thumb"><img src="{{url('public/uploads/'.$url.'/'.$data[0]->$image)}}" alt="">
								<figcaption>
									<div class="laywer-thumb-text">
										<h3>{{$data[0]->$title}}</h3>
									</div>
								</figcaption>
							</figure>
							<div class="lawyer-rich-editor">
								{{strip_tags($data[0]->$description)}}
							</div>
						</div>
						<aside class="col-md-3">
							@include('includes.sidebar')
						</aside>
					</div>
				</div>
			</div>
		</div>
		@include('includes.footer')
		<div class="clearfix"></div>
	</div>
	@include('includes.btm')