
<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from eyecix.com/html/lawyer/blog-detail-wls.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 May 2017 16:35:40 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>lawyer Blog Detail WLS</title>

    <!-- Css Files -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/flaticon.css" rel="stylesheet">
    <link href="css/slick-slider.css" rel="stylesheet">
    <link href="css/fancybox.css" rel="stylesheet">
    <link href="../../public/assets/css/style.css" rel="stylesheet">
    <link href="css/color.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	
    <!--// Main Wrapper \\-->
    <div class="lawyer-main-wrapper">

        <!--// Header \\-->
        <header id="lawyer-header" class="lawyer-header-one">
            
            <!--// TopStrip \\-->
            <div class="lawyer-top-strip lawyer-bgcolor">
                <div class="container">
                    <div class="row">
                        <aside class="col-md-6">
                            <ul class="lawyer-strip-info">
                                <li> <i class="fa fa-phone"></i> +123 45 678</li>
                                <li> <i class="fa fa-envelope-o"></i> <a href="mailto:name@email.com">info@example.com</a></li>
                            </ul>
                        </aside>
                        <aside class="col-md-6">
                            <div class="lawyer-right-section">
                                <div class="lawyer-language-switcher">
                                    <ul>
                                        <li><i class="fa fa-globe"></i> <a href="#">English</a>
                                            <ul class="lawyer-language-menu">
                                                <li><a href="#">English</a></li>
                                                <li><a href="#">Arabic</a></li>
                                                <li><a href="#">Farsi</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <form class="lawyer-search">
                                    <input value="Search" onblur="if(this.value == '') { this.value ='Search'; }" onfocus="if(this.value =='Search') { this.value = ''; }" type="text">
                                    <i class="fa fa-search"></i>
                                    <input type="submit" value="">
                                </form>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
            <!--// TopStrip \\-->

            <!--// MainHeader \\-->
            <div class="lawyer-mainheader">
                <div class="container">
                    <div class="row">
                        <aside class="col-md-3"><a href="index-2.html" class="lawyer-logo"><img src="images/logo.png" alt=""></a></aside>
                        <aside class="col-md-9">
                            <!--// Navigation \\-->
                            <nav class="navbar navbar-default">
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="true">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                </div>
                                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                                  <ul class="nav navbar-nav">
                                    <li class="active"><a href="index-2.html">Home</a></li>
                                    <li><a href="about-us.html">about us</a></li>
                                    <li><a href="#">Attorney</a>
                                        <ul class="lawyer-dropdown-menu">
                                            <li><a href="attorney-grid.html">Attorney Grid</a>
                                                <ul class="lawyer-dropdown-menu">
                                                    <li><a href="attorney-grid-wls.html">Attorney Grid W/L/S</a></li>
                                                    <li><a href="attorney-grid-wrs.html">Attorney Grid W/R/S</a></li>
                                                    <li><a href="attorney-grid.html">Attorney Grid W/O/S</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="attorney-modren.html">Attorney Modren</a>
                                                <ul class="lawyer-dropdown-menu">
                                                    <li><a href="attorney-modren-wls.html">Attorney Modren W/L/S</a></li>
                                                    <li><a href="attorney-modren-wrs.html">Attorney Modren W/R/S</a></li>
                                                    <li><a href="attorney-modren.html">Attorney Modren W/O/S</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="attorney-classic.html">Attorney Classic</a>
                                                <ul class="lawyer-dropdown-menu">
                                                    <li><a href="attorney-classic.html">Attorney Classic W/R/S</a></li>
                                                    <li><a href="attorney-classic-wls.html">Attorney Classic W/L/S</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="attorney-detail.html">Attorney Detail</a>
                                                <ul class="lawyer-dropdown-menu">
                                                    <li><a href="attorney-detail.html">Attorney Detail W/R/S</a></li>
                                                    <li><a href="attorney-detail-wls.html">Attorney Detail W/L/S</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="contact-us.html">Our News</a>
                                        <ul class="lawyer-dropdown-menu">
                                            <li><a href="blog-classic.html">News Classic</a>
                                                <ul class="lawyer-dropdown-menu">
                                                    <li><a href="blog-classic.html">News Classic W/O/S</a></li>
                                                    <li><a href="blog-classic-wls.html">News Classic W/L/S</a></li>
                                                    <li><a href="blog-classic-wrs.html">News Classic W/R/S</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="blog-grid.html">News Grid</a>
                                                <ul class="lawyer-dropdown-menu">
                                                    <li><a href="blog-grid.html">News Grid W/O/S</a></li>
                                                    <li><a href="blog-grid-wls.html">News Grid W/L/S</a></li>
                                                    <li><a href="blog-grid-wrs.html">News Grid W/R/S</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="blog-large.html">News Large</a>
                                                <ul class="lawyer-dropdown-menu">
                                                    <li><a href="blog-large.html">News Large W/R/S</a></li>
                                                    <li><a href="blog-large-wls.html">News Large W/L/S</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="blog-detail.html">News Detail</a>
                                                <ul class="lawyer-dropdown-menu">
                                                    <li><a href="blog-detail.html">News Detail W/R/S</a></li>
                                                    <li><a href="blog-detail-wls.html">News Detail W/L/S</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="lawyer-megamenu-li"><a href="#">Features</a>
                                        <ul class="lawyer-megamenu">
                                            <li class="row">
                                                <div class="col-md-2">
                                                    <h4>Link 1</h4>
                                                    <ul class="lawyer-megalist">
                                                        <li><a href="practice-medium.html">Practice Medium</a></li>
                                                        <li><a href="practice-grid.html">Practice Grid W/O/S</a></li>
                                                        <li><a href="practice-grid-wls.html">Practice Grid W/L/S</a></li>
                                                        <li><a href="practice-grid-wrs.html">Practice Grid W/R/S</a></li>
                                                        <li><a href="practice-modren.html">Practice Modren W/O/S</a></li>
                                                        <li><a href="practice-modren-wls.html">Practice Modren W/L/S</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-2">
                                                    <h4>Link 2</h4>
                                                    <ul class="lawyer-megalist">
                                                        <li><a href="practice-modren-wrs.html">Practice Modren W/R/S</a></li>
                                                        <li><a href="practice-serices-wls.html">Practice Services</a></li>
                                                        <li><a href="team-classic.html">Team Classic W/O/S</a></li>
                                                        <li><a href="team-classic-wls.html">Team Classic W/L/S</a></li>
                                                        <li><a href="team-classic-wrs.html">Team Classic W/R/S</a></li>
                                                        <li><a href="team-grid.html">Team Grid W/O/S</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-2">
                                                    <h4>Link 3</h4>
                                                    <ul class="lawyer-megalist">
                                                        <li><a href="team-grid-wls.html">Team Grid W/L/S</a></li>
                                                        <li><a href="team-grid-wrs.html">Team Grid W/R/S</a></li>
                                                        <li><a href="gallery-two-column.html">Gallery 2 Column</a></li>
                                                        <li><a href="gallery-three-column.html">Gallery 3 Column</a></li>
                                                        <li><a href="gallery.html">Gallery 4 Column</a></li>
                                                        <li><a href="search-result.html">Search Result</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="#" class="lawyer-thumbnail">
                                                        <img src="extra-images/megamenu-frame.jpg" alt="" />
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="lawyer-megamenu-li"><a href="#">Contact us</a>
                                        <ul class="lawyer-megamenu">
                                            <li class="row">
                                                <div class="col-md-2">
                                                    <h4>Links 1</h4>
                                                    <ul class="lawyer-megalist">
                                                        <li><a href="faq.html">Faq</a></li>
                                                        <li><a href="404.html">404 Error Page</a></li>
                                                        <li><a href="search-result.html">Search Result</a></li>
                                                        <li><a href="search-result-wls.html">Search Result W/L/S</a></li>
                                                        <li><a href="contact-us.html">Contact Us</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-5">
                                                    <h4>Artists text</h4>
                                                    <div class="lawyer-mega-text">
                                                        <p>Your work is going to fill a large part of your life, and the only way to be truly satisfied is to do what you believe is great work. And the only way to do great work is to love.</p>
                                                        <p>If you haven't found it yet, keep looking. Don't settle. As with all matters of the heart, you'll know when you find it.</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <h4>sub category widget</h4>
                                                    <a href="#" class="lawyer-thumbnail">
                                                        <img src="extra-images/mega-menuadd.jpg" alt="" />
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                  </ul>
                                </div>
                            </nav>
                            <!--// Navigation \\-->
                            <a href="#" class="lawyer-simple-btn lawyer-bgcolor">Free Consultation</a>
                        </aside>
                    </div>
                </div>
            </div>
            <!--// MainHeader \\-->

        </header>
        <!--// Header \\-->

        <!--// SubHeader \\-->
        <div class="lawyer-subheader">
            <div class="lawyer-subheader-image">
                <span class="lawyer-dark-transparent"></span>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1>Blog Detail W/L/Sidebar</h1>
                            <span>Vestibulum at lorem lacinia</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lawyer-breadcrumb">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ul>
                                <li><a href="index-2.html">Homepage </a></li>
                                <li>Blog Detail</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--// SubHeader \\-->

		<!--// Main Content \\-->
		<div class="lawyer-main-content">

            <!--// Main Section \\-->
            <div class="lawyer-main-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <figure class="lawyer-figure-thumb"><img src="extra-images/blog-detail-thumb.jpg" alt="">
                                <figcaption>
                                    <div class="laywer-thumb-text">
                                        <h3>Legal Issues Regarding Paternity</h3>
                                        <ul class="lawyer-detail-social">
                                            <li><a href="404.html"><i class="fa fa-user"></i>George Hamilton </a></li>
                                            <li><a href="404.html"><i class="fa fa-comment"></i>79 Comments </a></li>
                                            <li><a href="404.html"><i class="fa fa-calendar-o"></i> 21 August, 2017</a></li>
                                        </ul>
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="lawyer-rich-editor">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ac malesuada ante. Curabitur lacinia diam tempus tempor consectet Sed vitae dignissim purus, eget aliquam libero. Duis et arcu a erat venenatis ornare eget nec urna. Nulla volutpat luctus venen Aliquam tellus dui, luctus nec ornare at, aliquet ac nulla. Quisque vitae feugiat eros. Pellentesque tempus tortor nec tellus el ifend, id dictum nibh volutpat.</p>
                                <p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit condimentum odio. Suspendisse lacinia, risus et porta dignissim, elit tellus iaculis tellus, eget efficitur elit magna eu orci. Phasellus tempor consectetur magna, at efficit est malesuada ac. Phasellus non ipsum diam. Suspendisse potenti.</p>
                                <blockquote>
                                    <span><i class="fa fa-quote-right"></i></span>
                                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curael ullam Nullam port id efficitur condimentum, dui nisl ullamcorper diam, at molestie nulla erat egestas nisi. Ut quis ex et ma gna egestas tempor Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                </blockquote>
                                <p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit condimentum odio. Suspendisse lacinia risus et porta dignissim, elit tellus iaculis tellus, eget efficitur elit magna eu orci. Phasellus tempor consesacte magna, at efficit est malesuada ac. Phasellus non ipsum diam. Suspendisse potenti.</p>
                                <div class="lawyer-post-tags">
                                  <div class="lawyer-tags">
                                    <i class="fa fa-tag"></i>
                                     <a href="404.html">Family ,</a>
                                     <a href="404.html">Financial ,</a>
                                     <a href="404.html">Law ,</a>
                                     <a href="404.html">Lawyer</a>
                                  </div>
                                  <div class="lawyer-blog-social">
                                     <ul>
                                        <li><a href="https://www.facebook.com/" class="color-one"><i class="fa fa-facebook"></i>facebook</a></li>
                                        <li><a href="https://twitter.com/login" class="color-two"><i class="fa fa-twitter"></i>Twitter</a></li>
                                        <li><a href="https://plus.google.com/" class="color-three"><i class="fa fa-google-plus"></i>Google+</a></li>
                                     </ul>
                                  </div>
                               </div>
                            </div>
                            <div class="lawyer-prenxt-post">
                                <ul>
                                    <li>
                                        <div class="lawyer-prev-post">
                                            <div class="lawyer-prev-artical">
                                                <h3><a href="404.html">Vestibulum lorem lacinia, ultrices tortor in, porta nisl</a></h3>
                                                <a href="404.html" class="lawyer-post-arrow"><i class="fa fa-angle-double-left"></i> Previous Post</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="lawyer-next-post">
                                            <div class="lawyer-next-artical">
                                                <h3><a href="404.html">Lorem ipsum dolor sit amet, consectetu radipiscing elit</a></h3>
                                                <a href="404.html" class="lawyer-post-arrow">Next Post <i class="fa fa-angle-double-right"></i></a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="lawyer-section-heading"><h2>About Author</h2></div>
                            <div class="lawyer-admin-wrap">
                                <div class="lawyer-admin-post">
                                    <figure>
                                        <a href="404.html"><img src="extra-images/admin-post-img.jpg" alt=""></a>
                                    </figure>
                                    <div class="lawyer-admin-post-text">
                                        <h5><a href="404.html">Mark Wilson</a></h5>
                                        <span>Adminstrator</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor dignissim lectus, sed vehi auc cula lorem euismod id. Integer a sapien non augue viverra dapibus.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="lawyer-section-heading"><h2>Related Articles</h2></div>
                            <div class="lawyer-related-articles">
                                <ul class="row">
                                    <li class="col-md-6">
                                        <figure><a href="blog-detail.html"><img src="extra-images/related-post-img1.jpg" alt=""></a></figure>
                                        <div class="lawyer-related-articles-text">
                                            <h4><a href="blog-detail.html">Legal Issues Regarding </a></h4>
                                            <ul class="lawyer-classic-blog-option">
                                                <li><a href="404.html">George Hamilton / </a></li>
                                                <li><a href="404.html"> 79 Comments</a></li>
                                            </ul>
                                            <p>Lorem ipsum dolor sit amet, cons cing elit. Pellentesque congue, ar varius, nisi quam mollis mi.</p>
                                            <a href="blog-detail.html" class="lawyer-readmore-btn">Read More</a>
                                        </div>
                                    </li>
                                    <li class="col-md-6">
                                        <figure><a href="blog-detail.html"><img src="extra-images/related-post-img2.jpg" alt=""></a></figure>
                                        <div class="lawyer-related-articles-text">
                                            <h4><a href="blog-detail.html">Legal Issues Regarding </a></h4>
                                            <ul class="lawyer-classic-blog-option">
                                                <li><a href="404.html">George Hamilton / </a></li>
                                                <li><a href="404.html"> 79 Comments</a></li>
                                            </ul>
                                            <p>Lorem ipsum dolor sit amet, cons cing elit. Pellentesque congue, ar varius, nisi quam mollis mi.</p>
                                            <a href="blog-detail.html" class="lawyer-readmore-btn">Read More</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="comments-area">
                              <!--// coments \\-->
                              <div class="lawyer-section-heading"><h2>Comments</h2></div>
                              <ul class="comment-list">
                                 <li>
                                    <div class="thumb-list">
                                       <figure><img alt="" src="extra-images/comment-img1.jpg"><a class="comment-reply-link" href="#">Reply <i class="fa fa-share"></i></a></figure>
                                       <div class="text-holder">
                                          <h6>Mark Clarke</h6>
                                          <time class="post-date" datetime="2008-02-14 20:00">August 21, 2017 at 1:51 pm </time>
                                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor dignissim lectus, sed vehicula lorem euismo did. Integer a sapien non augue viverra dapibus at at sapien.</p>
                                       </div>
                                    </div>
                                    <div class="thumb-list">
                                       <figure><img alt="" src="extra-images/comment-img2.jpg"><a class="comment-reply-link" href="#">Reply <i class="fa fa-share"></i></a></figure>
                                       <div class="text-holder">
                                          <h6>Sarena Jones</h6>
                                          <time class="post-date" datetime="2008-02-14 20:00">November 21, 2017 at 1:51 pm </time>
                                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor dignissim lectus, sed vehicula lorem euismo did. Integer a sapien non augue viverra dapibus at at sapien.</p>
                                       </div>
                                    </div>
                                    <ul class="children">
                                       <li>
                                          <div class="thumb-list">
                                             <figure><img alt="" src="extra-images/comment-img3.jpg"></figure>
                                             <div class="text-holder">
                                                <h6>Julie Ann</h6>
                                                <time class="post-date" datetime="2008-02-14 20:00">December 21, 2017 at 1:51 pm </time>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor dignissim lectus, sed vehicula did. Integer a sapien non augue viverra dapibus at at sapien.</p>
                                             </div>
                                          </div>
                                       </li>
                                       <!-- #comment-## -->
                                    </ul>
                                    <!-- .children -->
                                 </li>
                                 <!-- #comment-## -->
                                 <li>
                                    <div class="thumb-list">
                                       <figure><img alt="" src="extra-images/comment-img4.jpg"><a class="comment-reply-link" href="#">Reply <i class="fa fa-share"></i></a></figure>
                                       <div class="text-holder">
                                          <h6>Mark Clarke</h6>
                                          <time class="post-date" datetime="2008-02-14 20:00">November 21, 2017 at 1:51 pm </time>
                                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor dignissim lectus, sed vehicula lorem euismo did. Integer a sapien non augue viverra dapibus at at sapien.</p>
                                       </div>
                                    </div>
                                 </li>
                                 <!-- #comment-## -->
                              </ul>
                              <!--// coments \\-->
                              <!--// comment-respond \\-->
                              <div class="comment-respond">
                                 <div class="lawyer-section-heading"><h2>Write A Comment</h2></div>
                                 <form>
                                    <p class="lawyer-full-form">
                                       <textarea name="comment" placeholder="Comment" class="commenttextarea"></textarea>
                                       <i class="fa fa-comment"></i>
                                    </p>
                                    <p>
                                       <input type="text" value="Name" onblur="if(this.value == '') { this.value ='Name'; }" onfocus="if(this.value =='Name') { this.value = ''; }">
                                       <i class="fa fa-user"></i>
                                    </p>
                                    <p>
                                       <input type="text" value="Website" onblur="if(this.value == '') { this.value ='Website'; }" onfocus="if(this.value =='Website') { this.value = ''; }">
                                       <i class="fa fa-globe"></i>
                                    </p>
                                    <p class="form-submit"><label><input class="lawyer-banner-btn lawyer-bgcolor" value="Submit Now" type="submit"></label> <input name="comment_post_ID" value="99" id="comment_post_ID" type="hidden">
                                    </p>
                                 </form>
                              </div>
                              <!--// comment-respond \\-->
                           </div>
                        </div>

                        <!--// SideBaar \\-->
                        <aside class="col-md-3">

                            <!--// Widget Search \\-->
                            <div class="lawyer-widget-heading"><h2>Search</h2></div>
                            <div class="widget widget_search">
                                <form>
                                    <input type="text" value="Type here" onblur="if(this.value == '') { this.value ='Type here'; }" onfocus="if(this.value =='Type here') { this.value = ''; }">
                                    <label>
                                        <input type="submit" value="">
                                    </label>
                                </form>
                            </div>
                            <!--// Widget Search \\-->

                            <!--// Widget Cetagories \\-->
                            <div class="lawyer-widget-heading"><h2>Cetagories</h2></div>
                            <div class="widget widget_cetagories">
                                <ul>
                                    <li>
                                        <figure><a href="404.html"><img src="extra-images/widget-cetagories-img.jpg" alt=""><i class="icon-auction"></i></a></figure>
                                    </li>
                                    <li>
                                        <figure><a href="404.html"><img src="extra-images/widget-cetagories-img.jpg" alt=""><i class="icon-libra"></i></a></figure>
                                    </li>
                                    <li>
                                        <figure><a href="404.html"><img src="extra-images/widget-cetagories-img.jpg" alt=""><i class="icon-money-bag"></i></a></figure>
                                    </li>
                                    <li>
                                        <figure><a href="404.html"><img src="extra-images/widget-cetagories-img.jpg" alt=""><i class="icon-diploma2"></i></a></figure>
                                    </li>
                                    <li>
                                        <figure><a href="404.html"><img src="extra-images/widget-cetagories-img.jpg" alt=""><i class="icon-auction"></i></a></figure>
                                    </li>
                                    <li>
                                        <figure><a href="404.html"><img src="extra-images/widget-cetagories-img.jpg" alt=""><i class="icon-libra"></i></a></figure>
                                    </li>
                                </ul>
                            </div>

                            <!--// Widget Cetagories \\-->

                            <!--// Widget archive \\-->
                            <div class="lawyer-widget-heading"><h2>Calendar Archives</h2></div>
                            <div class="widget widget_archive">
                              <ul>
                                 <li><a href="#"><i class="fa fa-sort-desc"></i>May 2017</a></li>
                                 <li><a href="#"><i class="fa fa-sort-desc"></i>August 2017</a></li>
                                 <li><a href="#"><i class="fa fa-sort-desc"></i>September 2017</a></li>
                                 <li><a href="#"><i class="fa fa-sort-desc"></i>November 2017</a></li>
                                 <li><a href="#"><i class="fa fa-sort-desc"></i>December 2017</a></li>
                              </ul>
                            </div>

                            <!--// Widget archive \\-->

                            <!--// Widget archive \\-->
                            <div class="lawyer-widget-heading"><h2>Popular Posts</h2></div>
                            <div class="widget widget_populer_posts">
                                <ul>
                                    <li>
                                        <figure><a href="blog-detail.html"><img src="extra-images/widget-post-img1.jpg" alt=""><i class="fa fa-angle-double-right"></i></a></figure>
                                        <section>
                                            <h6><a href="blog-detail.html">Lorem ipsum dolor sita met, consecte</a></h6>
                                            <time datetime="2008-02-14 20:00"><i class="fa fa-calendar-o"></i>15 May, 2017</time>
                                        </section>
                                    </li>
                                    <li>
                                        <figure><a href="blog-detail.html"><img src="extra-images/widget-post-img2.jpg" alt=""><i class="fa fa-angle-double-right"></i></a></figure>
                                        <section>
                                            <h6><a href="blog-detail.html">Lorem ipsum dolor sita met, consecte</a></h6>
                                            <time datetime="2008-02-14 20:00"><i class="fa fa-calendar-o"></i>15 May, 2017</time>
                                        </section>
                                    </li>
                                    <li>
                                        <figure><a href="blog-detail.html"><img src="extra-images/widget-post-img3.jpg" alt=""><i class="fa fa-angle-double-right"></i></a></figure>
                                        <section>
                                            <h6><a href="blog-detail.html">Lorem ipsum dolor sita met, consecte</a></h6>
                                            <time datetime="2008-02-14 20:00"><i class="fa fa-calendar-o"></i>15 May, 2017</time>
                                        </section>
                                    </li>
                                </ul>
                            </div>
                            <!--// Widget archive \\-->

                            <!--// Widget Awards \\-->
                            <div class="lawyer-widget-heading"><h2>Attorneys</h2></div>
                            <div class="widget widget widget_attorney">
                                <div>
                                    <figure><a href="attorney-detail.html"><img src="extra-images/widget-attorneys-img1.jpg" alt=""></a></figure>
                                    <div class="widget-attorney-text">
                                        <h6><a href="attorney-detail.html">John Layfield</a></h6>
                                        <span>Lawyer Cetagory</span>
                                    </div>
                                </div>
                                <div>
                                    <figure><a href="attorney-detail.html"><img src="extra-images/widget-attorneys-img2.jpg" alt=""></a></figure>
                                    <div class="widget-attorney-text">
                                        <h6><a href="attorney-detail.html">John Layfield</a></h6>
                                        <span>Lawyer Cetagory</span>
                                    </div>
                                </div>
                            </div>
                            <!--// Widget Awards \\-->

                        </aside>
                        <!--// SideBaar \\-->

                    </div>
                </div>
            </div>
            <!--// Main Section \\-->

		</div>
		<!--// Main Content \\-->

		<!--// Footer \\-->
        <footer id="lawyer-footer" class="lawyer-footer-one">
            
            <!--// Footer Widget \\-->
            <div class="lawyer-footer-widget">
                <div class="container">
                    <div class="row">
                        <!--// Widget Links \\-->
                        <aside class="col-md-4 widget widget_links">
                            <div class="footer-widget-title"><h2>Useful Links</h2></div>
                            <ul>
                                <li><a href="404.html">Photography</a></li>
                                <li><a href="404.html">Electronics</a></li>
                                <li><a href="404.html">Terms & Conditions</a></li>
                                <li><a href="404.html">Privacy policy</a></li>
                                <li><a href="404.html">Marketing</a></li>
                                <li><a href="404.html">Site Map</a></li>
                                <li><a href="404.html">Featured Listing</a></li>
                                <li><a href="404.html">Latest News</a></li>
                                <li><a href="404.html">Photography</a></li>
                                <li><a href="404.html">Explore</a></li>
                                <li><a href="404.html">Site Map</a></li>
                                <li><a href="404.html">Electronics</a></li>
                                <li><a href="404.html">Terms & Conditions</a></li>
                                <li><a href="404.html">Privacy policy</a></li>
                            </ul>
                        </aside>
                        <!--// Widget Links \\-->

                        <!--// Widget Populer Posts \\-->
                        <aside class="col-md-4 widget widget_populer_posts">
                            <div class="footer-widget-title"><h2>Populer Posts</h2></div>
                            <ul>
                                <li>
                                    <figure><a href="blog-detail.html"><img src="extra-images/widget-post-img1.jpg" alt=""><i class="fa fa-angle-double-right"></i></a></figure>
                                    <section>
                                        <h6><a href="blog-detail.html">Vestibulum at lorem lacinia, ultric es tortor in, porta nisl</a></h6>
                                        <time datetime="2008-02-14 20:00"><i class="fa fa-calendar-o"></i>21 Aug, 2017</time>
                                    </section>
                                </li>
                                <li>
                                    <figure><a href="blog-detail.html"><img src="extra-images/widget-post-img2.jpg" alt=""><i class="fa fa-angle-double-right"></i></a></figure>
                                    <section>
                                        <h6><a href="blog-detail.html">Vestibulum at lorem lacinia, ultric es tortor in, porta nisl</a></h6>
                                        <time datetime="2008-02-14 20:00"><i class="fa fa-calendar-o"></i>21 Aug, 2017</time>
                                    </section>
                                </li>
                                <li>
                                    <figure><a href="blog-detail.html"><img src="extra-images/widget-post-img3.jpg" alt=""><i class="fa fa-angle-double-right"></i></a></figure>
                                    <section>
                                        <h6><a href="blog-detail.html">Vestibulum at lorem lacinia, ultric es tortor in, porta nisl</a></h6>
                                        <time datetime="2008-02-14 20:00"><i class="fa fa-calendar-o"></i>21 Aug, 2017</time>
                                    </section>
                                </li>
                            </ul>
                        </aside>
                        <!--// Widget Populer Posts \\-->

                        <!--// Widget Our Newsletter \\-->
                        <aside class="col-md-4 widget widget_newsletter">
                            <div class="footer-widget-title"><h2>Our Newsletter</h2></div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscinge lit. Sed auctor dignissim lectus.</p>
                            <form>
                                <ul>
                                    <li>
                                    <label>Email:</label>
                                    <input type="email" value="Type Here" onblur="if(this.value == '') { this.value ='Type Here'; }" onfocus="if(this.value =='Type Here') { this.value = ''; }">
                                    </li>
                                    <li>
                                        <label>Website:</label>
                                        <input type="email" value="Type Here" onblur="if(this.value == '') { this.value ='Type Here'; }" onfocus="if(this.value =='Type Here') { this.value = ''; }">
                                    </li>
                                    <li><label><input class="lawyer-banner-btn lawyer-bgcolor" type="submit" value="Submit Now"></label></li>
                                </ul>
                            </form>
                        </aside>
                        <!--// Widget Our Newsletter \\-->

                        <!--// Footer Contact Us \\-->
                        <div class="col-md-12">
                            <div class="lawyer-contact-us">
                                <ul class="row">
                                    <li class="col-md-3">
                                        <div class="lawyer-contact-us-text">
                                            <i class="fa fa-phone lawyer-bgcolor"></i>
                                            <h5>Call Us At</h5>
                                            <span>+123 45 678</span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="lawyer-contact-us-text">
                                            <i class="fa fa-envelope lawyer-bgcolor"></i>
                                            <h5>Mail Us At</h5>
                                            <span><a href="mailto:name@email.com">info@example.com</a></span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="lawyer-contact-us-text">
                                            <i class="fa fa-map-marker lawyer-bgcolor"></i>
                                            <h5>our location</h5>
                                            <span>1403 Blackwell Street, AK 99</span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="lawyer-contact-us-text">
                                            <i class="fa fa-fax lawyer-bgcolor"></i>
                                            <h5>Our Fax Is</h5>
                                            <span>001 (407) 901-6400 </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--// Footer Contact Us \\-->

                        <!--// CopyRight \\-->
                        <div class="col-md-12">
                            <div class="lawyer-copyright">
                                <p><i class="fa fa-copyright"></i> 2016, All Right Reserved - by <a href="index-2.html">EyeCix</a></p>
                                <a href="#" class="lawyer-back-top lawyer-bgcolor"><i class="fa fa-angle-up"></i></a>
                            </div>
                        </div>
                        <!--// CopyRight \\-->

                    </div>
                </div>
            </div>
            <!--// Footer Widget \\-->

        </footer>
        <!--// Footer \\-->

	<div class="clearfix"></div>
    </div>
    <!--// Main Wrapper \\-->

	<!-- jQuery (necessary for JavaScript plugins) -->
	<script type="text/javascript" src="script/jquery.js"></script>
    <script type="text/javascript" src="script/bootstrap.min.js"></script>
    <script type="text/javascript" src="script/slick.slider.min.js"></script>
    <script type="text/javascript" src="script/fancybox.pack.js"></script>
    <script type="text/javascript" src="script/isotope.min.js"></script>
    <script type="text/javascript" src="script/progressbar.js"></script>
    <script type="text/javascript" src="script/circle-chart.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
    <script type="text/javascript" src="script/functions.js"></script>

  </body>

<!-- Mirrored from eyecix.com/html/lawyer/blog-detail-wls.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 29 May 2017 16:35:40 GMT -->
</html>