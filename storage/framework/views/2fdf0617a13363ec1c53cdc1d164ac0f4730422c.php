	<script src="<?php echo e(url('public/assets/script/jquery.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(url('public/assets/script/bootstrap.min.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(url('public/assets/script/slick.slider.min.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(url('public/assets/script/fancybox.pack.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(url('public/assets/script/isotope.min.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(url('public/assets/script/progressbar.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(url('public/assets/script/circle-chart.js')); ?>" type="text/javascript"></script>
	<script src="<?php echo e(url('public/assets/script/functions.js')); ?>" type="text/javascript"></script>
</body>
</html>