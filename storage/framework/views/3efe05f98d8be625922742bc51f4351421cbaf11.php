<!DOCTYPE html>
<html class="fixed">
<head>
    <meta charset="UTF-8">
    <title>Dashboard |Legal Research Associates</title>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta content="Karma Tech Solutions Admin panel" name="keywords">
    <meta content="CMS for Hello Himalayan Homes by Karma Tech Solutions" name="description">
    <meta content="Karma Tech Solutions" name="author">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/vendor/bootstrap/css/bootstrap.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/vendor/font-awesome/css/font-awesome.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/vendor/magnific-popup/magnific-popup.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/vendor/select2/css/select2.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/vendor/jquery-datatables-bs3/assets/css/datatables.css')); ?>"/>
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/vendor/select2-bootstrap-theme/select2-bootstrap.min.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/vendor/bootstrap-toggle/bootstrap-toggle.min.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/stylesheets/theme.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/stylesheets/theme-custom.css')); ?>">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo e(url('public/admin-assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')); ?>" />
    <script
  src="https://code.jquery.com/jquery-3.2.1.js"></script>
    <script src="<?php echo e(url('public/admin-assets/vendor/ckeditor/ckeditor.js')); ?>"></script>
    <script src="<?php echo e(url('public/admin-assets/vendor/modernizr/modernizr.js')); ?>"></script>
    
</head>
<header class="header">
    <div class="logo-container">
        <a class="logo" href="<?php echo e(url('admin/dashboard')); ?>">
            <img alt="Porto Admin" height="35" src="<?php echo e(url('public/assets/img/logo.png')); ?>">
        </a>
        <div class="visible-xs toggle-sidebar-left" data-fire-event="sidebar-left-opened" data-target="html" data-toggle-class="sidebar-left-opened">
            <i aria-label="Toggle sidebar" class="fa fa-bars"></i>
        </div>
    </div>
    <div class="header-right">

        <span class="separator"></span>
        <div class="userbox" id="userboxi">
            <a data-toggle="dropdown" href="#">
                <figure class="profile-picture">
                    <i class="img-circle fa fa-user"></i>
                </figure>
                <div class="profile-info" data-lock-email="" data-lock-name="">
                    <span class="name"><?php if(!empty(Session::get('user'))): ?> <?php echo e(Session::get('user')); ?> <?php endif; ?></span> <span class="role"><a  href="<?php echo e(url('admin/logout')); ?>"><i class="fa fa-power-off"></i> Logout</a></span>
                </div>
                <i class="fa custom-caret"></i>
            </a>
            <div class="dropdown-menu">
                <ul class="list-unstyled">
                    <li class="divider"></li>
                    <li>
                        <a href="#" role="menuitem" tabindex="-1"><i class="fa fa-user"></i> My Profile</a>
                    </li>
                    <li>
                        <a href="<?php echo e(url('admin/logout')); ?>" role="menuitem" tabindex="-1"><i class="fa fa-power-off"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>