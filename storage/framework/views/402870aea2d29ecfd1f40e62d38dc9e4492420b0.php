<?php ($pgName = 'Our Services | Legal Research Associats'); ?>
	<?php echo $__env->make('includes.top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="lawyer-main-wrapper">
		<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="lawyer-subheader">
			<div class="lawyer-subheader-image">
				<span class="lawyer-dark-transparent"></span>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1>Our Services</h1>
							<span></span>
						</div>
					</div>
				</div>
			</div>
			<div class="lawyer-breadcrumb">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><a href="<?php echo e(url('home')); ?>">Home </a></li>
								<li>Services</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lawyer-main-content">
			<div class="lawyer-main-section">
				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<div class="lawyer-fancy-title lawyer-fancy-titleleft">
                                <h2>Our <span class="lawyer-color"> Services</span></h2>
                                <span>What We Do<small></small></span>
                            </div>
                            <div class="lawyer-practice lawyer-practice-Services">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor dignissim lect, sed vehicula lorem euismod id. Integer a sapien non augue viverra dapibus at at sapien. Inte tempus enim in augue semper bibendum.</p>
                                <ul class="row">
									<?php if($data!=0): ?>

										<?php foreach($data as $value): ?>
											<?php ($title = $url."_title"); ?>
											<?php ($description = $url."_description"); ?>
											<?php ($image = $url."_image"); ?>
                                    <li class="col-md-6">
                                        <div class="lawyer-practice-wrap lawyer-divorce-law">
                                            <span class="practices-transparent"></span>
                                            <div class="lawyer-practice-text">
                                                <h6><i class=" icon-gun"></i><a href="<?php echo e(url($url.'-detail/'.$url.'/'.$value->ID)); ?>"><?php echo e($value->$title); ?></a></h6>
                                                <p><?php echo e(strip_tags(substr($value->$description,0,68))); ?></p>
                                            </div>
                                        </div>
                                    </li>
										<?php endforeach; ?>
									<?php endif; ?>
                                </ul>
                            </div>
						</div>
						<aside class="col-md-3">
							<?php echo $__env->make('includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
						</aside>
					</div>
				</div>
			</div>
		</div>
		<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="clearfix"></div>
	</div>
	<?php echo $__env->make('includes.btm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>