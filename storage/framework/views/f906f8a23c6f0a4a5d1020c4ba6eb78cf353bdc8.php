						<div class="lawyer-widget-heading"><h2>Services</h2></div>
							<div class="widget widget_cetagories">
								<ul>
									<li><figure><a href="services-detail.php" title="Consultation"><img src="extra-images/widget-cetagories-img.jpg" alt=""><i class="icon-auction"></i></a></figure></li>
									<li><figure><a href="services-detail.php" title="Banking"><img src="extra-images/widget-cetagories-img.jpg" alt=""><i class="icon-diploma2"></i></a></figure></li>
									<li><figure><a href="services-detail.php" title="Finance"><img src="extra-images/widget-cetagories-img.jpg" alt=""><i class="icon-money-bag"></i></a></figure></li>
									<li><figure><a href="services-detail.php" title="Contracts"><img src="extra-images/widget-cetagories-img.jpg" alt=""><i class="icon-libra"></i></a></figure></li>
									<li><figure><a href="services-detail.php" title="Joint Venture"><img src="extra-images/widget-cetagories-img.jpg" alt=""><i class="icon-auction"></i></a></figure></li>
									<li><figure><a href="services-detail.php" title="Taxation"><img src="extra-images/widget-cetagories-img.jpg" alt=""><i class="icon-diploma2"></i></a></figure></li>
								</ul>
							</div>
							<div class="lawyer-widget-heading"><h2>Popular Posts</h2></div>
							<div class="widget widget_populer_posts">
								<ul>
									<?php ($news = \App\Http\Controllers\admin\NewsController::StaticNews()); ?>
									<?php if($news!=0): ?>
									<?php foreach($news as $value): ?>
									<li>
										<figure><a href="<?php echo e(url('news-detail/'.$value->ID)); ?>"><img src="<?php echo e(url('public/uploads/newss/'.$value->news_image)); ?>" alt=""><i class="fa fa-angle-double-right"></i></a></figure>
										<section>
											<h6><a href="<?php echo e(url('news-detail/'.$value->ID)); ?>"><?php echo e($value->news_title); ?></a></h6>
											<time datetime="2008-02-14 20:00"><i class="fa fa-calendar-o"></i><?php echo e($value->created_date); ?></time>
										</section>
									</li>
										<?php endforeach; ?>
										<?php endif; ?>
								</ul>
							</div>
							<div class="lawyer-widget-heading"><h2>Attorneys</h2></div>
							<div class="widget widget widget_attorney">
								<?php ($team = \App\Http\Controllers\admin\TeamController::StaticTeam()); ?>
								<?php if($team!=0): ?>
									<?php foreach($team as $value): ?>
								<div>
									<figure><a href="<?php echo e(url('team-detail/'.$value->ID)); ?>"><img src="<?php echo e(url('public/uploads/team/'.$value->member_image)); ?>" alt=""></a></figure>
									<div class="widget-attorney-text">
										<h6><a href="<?php echo e(url('team-detail/'.$value->ID)); ?>"><?php echo e($value->member_name); ?></a></h6>
										<span><?php echo e($value->member_category); ?></span>
									</div>
								</div>
									<?php endforeach; ?>
								<?php endif; ?>
								<div>
									<figure><a href="team-detail.php"><img src="<?php echo e(url('public/assets/img/lawyer2.jpg')); ?>" alt=""></a></figure>
									<div class="widget-attorney-text">
										<h6><a href="team-detail.php">Umesh Pd. Sitaula</a></h6>
										<span>Lawyer Cetagory</span>
									</div>
								</div>
							</div>